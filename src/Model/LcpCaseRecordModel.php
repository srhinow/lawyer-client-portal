<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\Model;

/*
 * @author     Sven Rhinow
 * @package    srhinow/lawyer-client-portal
 * @filesource
 */

use Contao\Model;

class LcpCaseRecordModel extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_lcp_case_record';

    /**
     * findet alle gerade noch offenen Fälle zu dem BeUser (sollte im Idealfall nur einer sein).
     *
     * @return |null
     */
    public function findAllNewMemberRecords()
    {
        $t = self::$strTable;
        $arrColumns[] = "$t.authorTable=?";
        $arrValues[] = 'tl_member';

        $arrColumns[] = "$t.new=?";
        $arrValues[] = 1;

        return parent::findBy($arrColumns, $arrValues);
    }

    /**
     * @param $memberId
     * @param array  $columns
     * @param string $search
     * @param int    $intLimit
     * @param int    $intOffset
     * @param array  $arrOptions
     *
     * @return Model\Collection|LcpCaseRecordModel|null
     */
    public static function findPostedRecordsByMember(
        $memberId,
        $columns = [],
        $search = '',
        $intLimit = 0,
        $intOffset = 0,
        $arrOptions = []
    ) {
        if (empty($memberId)) {
            return null;
        }

        $t = static::$strTable;

        $arrValues = null;

        // nach aktuellem Mitglied filtern
        $arrColumns[] = "$t.`memberId`=".$memberId;

        // veroeffentlicht
        $arrColumns[] = "$t.`draft` = ''";
        $arrColumns[] = "$t.`dateSend` IS NOT NULL";

        // nur nicht geloeschte nachrichten
        $arrColumns[] = "$t.`deleted` = ''";

        //Wenn weitere Filter übergeben wurden z.B. case=1
        if (\is_array($columns) && \count($columns) > 0) {
            foreach ($columns as $ck => $cv) {
                $arrColumns[] = "$t.`$ck`=".$cv;
            }
        }

        //wenn was im Filter-Suchfeld übergeben wurde
        if (\strlen($search) > 0) {
            $arrFields = ['subject', 'text', 'authorName'];
            $search = strtolower(trim($search));
            $orColumns = [];

            foreach ($arrFields as $field) {
                $orColumns[] = "LOWER($t.`$field`) LIKE ?";
                $arrValues[] = '%'.$search.'%';
            }

            if (\count($orColumns) > 0) {
                $arrColumns[] = '('.implode(' OR ', $orColumns).')';
            }
        }

        if (!isset($arrOptions['order'])) {
            $arrOptions['order'] = "$t.dateAdded DESC";
        }

        $arrOptions['limit'] = $intLimit;
        $arrOptions['offset'] = $intOffset;

        return static::findBy($arrColumns, $arrValues, $arrOptions);
    }

    /**
     * @param $memberId
     *
     * @return Model\Collection|LcpCaseRecordModel|null
     */
    public static function findDraftRecordsByMember($memberId)
    {
        if (empty($memberId)) {
            return null;
        }

        $t = static::$strTable;

        // nach aktuellem Mitglied filtern
        $arrColumns[] = "$t.`memberId`=".$memberId;

        // veroeffentlicht
        $arrColumns[] = "$t.`draft` = '1'";

        $arrOptions['order'] = "$t.dateAdded DESC";

        return static::findOneBy($arrColumns, null, $arrOptions);
    }

    /**
     * Count all posted records to member by case.
     *
     * @param array $arrOptions An optional options array
     *
     * @return int The number of all relavant Records From Case
     */
    public static function countAllPostedCaseRecords($objCase, array $arrOptions = [])
    {
        if (!\is_object($objCase) || null === $objCase) {
            return null;
        }

        $t = static::$strTable;

        $arrColumns[] = "$t.`pid`=".$objCase->id;
//        $arrColumns[] = "$t.`authorTable`='tl_user'";
        $arrColumns[] = "$t.`dateSend` IS NOT NULL";
        $arrColumns[] = "$t.`draft` = ''";

        // nur nicht geloeschte nachrichten
        $arrColumns[] = "$t.`deleted` = ''";

        return static::countBy($arrColumns, null, $arrOptions);
    }

    /**
     * Count all new posted records to member by case.
     *
     * @param int   $memberId
     * @param array $arrOptions An optional options array
     *
     * @return int The number of all new Case Records
     */
    public static function countAllNewPostedRecordsByMemberId($memberId, array $arrOptions = [])
    {
        $t = static::$strTable;

        $arrColumns[] = "$t.`memberId`=".$memberId;
        $arrColumns[] = "$t.`new`= '1'";
        $arrColumns[] = "$t.`authorTable`='tl_user'";
        $arrColumns[] = "$t.`dateSend` IS NOT NULL";

        return static::countBy($arrColumns, null, $arrOptions);
    }

    /**
     * Count all posted records to member by case.
     *
     * @param int    $memberId
     * @param array  $columns
     * @param string $search
     * @param array  $arrOptions An optional options array
     *
     * @return int The number of all new Case Records
     */
    public static function countAllPostedRecordsByMemberId(
        $memberId,
        $columns = [],
        $search = '',
        $arrOptions = []
    ) {
        $t = static::$strTable;
        $arrValues = null;

        $arrColumns[] = "$t.`memberId`=".$memberId;
        $arrColumns[] = "$t.`draft` = ''";
        $arrColumns[] = "$t.`deleted` = ''";
        $arrColumns[] = "$t.`dateSend` IS NOT NULL";

        //Wenn weitere Filter übergeben wurden z.B. case=1
        if (\is_array($columns) && \count($columns) > 0) {
            foreach ($columns as $ck => $cv) {
                $arrColumns[] = "$t.`$ck`=".$cv;
            }
        }

        //wenn was im Filter-Suchfeld übergeben wurde
        if (\strlen($search) > 0) {
            $arrFields = ['subject', 'text', 'authorName'];
            $search = strtolower(trim($search));
            $orColumns = [];

            foreach ($arrFields as $field) {
                $orColumns[] = "LOWER($t.`$field`) LIKE ?";
                $arrValues[] = '%'.$search.'%';
            }

            if (\count($orColumns) > 0) {
                $arrColumns[] = '('.implode(' OR ', $orColumns).')';
            }
        }

        return static::countBy($arrColumns, $arrValues, $arrOptions);
    }

    /**
     * Count all new posted records to member by case.
     *
     * @param array $arrOptions An optional options array
     *
     * @return int The number of all new Case Records
     */
    public static function countNewPostedCaseRecords($objCase, array $arrOptions = [])
    {
        if (!\is_object($objCase) || null === $objCase) {
            return null;
        }

        $t = static::$strTable;

        $arrColumns[] = "$t.`pid`=".$objCase->id;
        $arrColumns[] = "$t.`new`= '1'";
        $arrColumns[] = "$t.`authorTable`='tl_user'";
        $arrColumns[] = "$t.`dateSend` IS NOT NULL";

        return static::countBy($arrColumns, null, $arrOptions);
    }
}
