<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\Model;

/*
 * @author     Sven Rhinow
 * @package    srhinow/lawyer-client-portal
 * @filesource
 */

use Contao\Model;

class LcpCaseModel extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_lcp_case';

    /**
     * findet alle gerade noch offenen Fälle zu dem BeUser (sollte im Idealfall nur einer sein).
     *
     * @param null $BeUserId
     *
     * @return Model|null
     */
    public function findCurrentOpenCases($BeUserId = null)
    {
        if (null === $BeUserId) {
            return null;
        }

        $arrColumns = ['dialer_author='.$BeUserId];

        return parent::findBy($arrColumns, null);
    }

    /**
     * zählt Fälle in dem aktuellen Monat.
     *
     * @return int|null
     */
    public function countCasesOnDateFormat($month = 0, $strFormat = '%Y-%m', $onlyWithSid = true)
    {
        $t = static::$strTable;

        if (0 === $month) {
            $month = date('n');
        }

        $arrColumns = ["DATE_FORMAT(FROM_UNIXTIME(`dateAdded`),'".$strFormat."') = '".$month."'"];
        if ($onlyWithSid) {
            $arrColumns[] = 'sid !=""';
        }

        return parent::countBy($arrColumns, null);
    }

    /**
     * @param $memberId
     * @param int $intLimit
     * @param int $intOffset
     *
     * @return Model\Collection|LcpCaseModel|null
     */
    public static function findPublishedByMember($memberId, $intLimit = 0, $intOffset = 0, array $arrOptions = [])
    {
        if (empty($memberId)) {
            return null;
        }

        $t = static::$strTable;

        // nach  aktuellem Mitglied filtern
        $arrColumns = ["$t.memberId=".$memberId];

        // veroeffentlicht
        $arrColumns[] = "$t.published='1'";

        if (!isset($arrOptions['order'])) {
            $arrOptions['order'] = "$t.dateAdded DESC";
        }

        $arrOptions['limit'] = $intLimit;
        $arrOptions['offset'] = $intOffset;

        return static::findBy($arrColumns, null, $arrOptions);
    }

    /**
     * Count published invoices by their member (Frontend-User) ID.
     *
     * @param array $arrOptions An optional options array
     *
     * @return int The number of Invoice items
     */
    public static function countPublishedByMember($memberId, array $arrOptions = [])
    {
        if (empty($memberId)) {
            return null;
        }

        $t = static::$strTable;

        // Rechnnung nach  aktuellem Mitglied filtern
        $arrColumns = ["$t.memberId=".$memberId];

        // veroeffentlicht
        $arrColumns[] = "$t.published='1'";

        return static::countBy($arrColumns, null, $arrOptions);
    }
}
