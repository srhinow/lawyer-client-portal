<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\Model;

/*
 * @author     Sven Rhinow
 * @package    srhinow/lawyer-client-portal
 * @filesource
 */

use Contao\Model;

class LcpSettingModel extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_lcp_setting';
}
