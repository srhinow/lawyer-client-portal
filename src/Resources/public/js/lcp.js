//===================================
// COOKIE-Funktions
//===================================
function setCookie(cname, cvalue, exdays)
{
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname)
{
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

//===================================
// LCP-Functions
//===================================

/**
 * Listen-Sortier-Funktion verarbeiten
 * @returns {boolean}
 */
function initSortingField()
{
    var sortField = $('select[name=sorting]');
    if (!sortField.length) {
        return false;
    }

    sortField.on('change', function (e) {
        $(this).parents('form')[0].submit();
    });
}

/**
 * bei der Verarbeitung als Mutli-Action nen Warnhinweis ausgeben wenn die Aktion "löschen" ist
 */
function initMultiActions()
{
    $('form[name=item_list_form]').on('submit', function (e) {
        if ($(this).find('select[name=action]')[0].value === 'delete') {
            if (!confirm('Sind Sie sicher, dass Sie alle markierten Nachrichten löschen möchten? ' +
                'Bitte beachten Sie, dass Sie nur Ihre eigenen Nachrichten löschen können.')) {
                e.preventDefault();
            }
        }
    });
}

/**
 * Den Button-click-Event zum löschen einer Nachricht verarbeiten.
 */
function initItemDeleteBtn()
{
    let delBtn = $('.detail_actions .btn_delete');
    let form = $(delBtn).parents('form')[0];

    delBtn.on('click', function (e) {
        e.preventDefault();
        if (confirm('Soll der Eintrag wirklich gelöscht werden?')) {
            let recordId = parseInt($(this).data('record'));

            //die Action-Checkbox zu diesem Record-Eintrag selectieren
            let itemCheckbox = $('#record_select_' + recordId);
            itemCheckbox.prop('checked', true);

            // das Action-Select-Feld auf 'löschen' setzen
            let actionSelect = $(form).find('select[name=action]')[0];
            $(actionSelect).val('delete');

            // Das Formular absenden
            form.submit();
        }
    });
}

/**
 * wenn es ein gespeicherten Entwurf gibt in den Nachrichtendetails als Button "neue Nachrichten"
 * dieses als Mledung ausgeben und wenn bestätigt, das Formular mit den "Entwurf verwwerfen" absenden.
 */
function initDropDraftAndNewBtn()
{
    let dropLink = $('.drop_draft_and_new');
    let DropDraftFormBtn = $('form[name=new_record_form] input[name=dropdraft]');

    // wenn nach dem reload noch der Cookie besteht
    let dan_cookie = getCookie('DROPANDNEW');
    if (dan_cookie !== '') {
        let nrButton = $('.details_'+dan_cookie+' .btn_new_record');
        if (nrButton.length) {
            setAnswerRecord(nrButton);
        }
        // fuer das naechste mal loeschen
        setCookie('DROPANDNEW', null, -1);
    }

    dropLink.on('click', function (e) {
        e.preventDefault();
        if (confirm('Achtung: Sie haben derzeit einen Entwurf gespeichert. ' +
            'Wenn Sie eine neue Nachricht verfassen, wird Ihr gespeicherter Entwurf verworfen')) {
            setCookie('DROPANDNEW',$(this).data('record'),1);
            setCookie('LCP_NEW_RECORD', 1, 1);
            $(DropDraftFormBtn).trigger('click');
        }
    });
}

/**
 * wenn "Akte downloaden" geklickt wurde, passendes Feld ins Formular einfügen
 * und zur Verarbeitung das Formular absenden
 * @returns {boolean}
 */
function initCaseDownloadBtn()
{
    let dlBtn = $('.detail_actions .btn_case_download');
    if (!dlBtn.length) {
        return false;
    }

    let form = $(dlBtn).parents('form')[0];

    dlBtn.on('click', function (e) {
        e.preventDefault();

        let caseId = parseInt($(this).data('caseid'));

        $('<input>').attr({
            type: 'hidden',
            id: 'foo',
            name: 'download_one',
            value: caseId
        }).appendTo(form);

        // Das Formular absenden
        form.submit();

    });
}

/**
 * mit einer checkbox alle listen-checkboxen checken/unchecken
 * @returns {boolean}
 */
function initCheckall()
{
    var checkall = $('input[name=checkall]');
    var items = $('input[class=item]');
    if (checkall.length < 1 || items.length < 1) {
        return false;
    }


    checkall.on('change', function (e) {
        var check = !!($(this).prop('checked'));

        items.each(function (k) {
                $(items[k]).prop('checked',check);
        });
    });
}

/**
 * je nachdem ob der Cookie gesetzt ist die Detailansicht ausklappen
 */
function initNewRecordContainer()
{
    let nrcookie = parseInt(getCookie('LCP_NEW_RECORD'));

    if (nrcookie === 1) {
        $('.btn_new_record').hide();
        showNewRecord();
    }
}

/**
 * neue Nachrichten-Button
 * @returns {boolean}
 */
function initNewRecordBtn()
{
    let newBtn = $('.btn_new_record');

    if (newBtn.length < 1) {
        return false;
    }

    newBtn.on('click', function (e) {
        e.preventDefault();
        newBtn.hide();
        setAnswerRecord(this);
        showNewRecord();
    });
}

/**
 * den "neue Nachricht" Bereich ausblenden
 * @returns {boolean}
 */
function initNewRecordCloseBtn()
{
    let closeBtn = $('.new_record .close');
    let newBtn = $('.btn_new_record');

    if (closeBtn.length < 1) {
        return false;
    }

    closeBtn.on('click', function (e) {
        e.preventDefault();

        hideNewRecord();
        newBtn.show();
    });
}

/**
 * Antworten-Button in den Nachrichtendetails um "neue Nachrichten-Formular" vorzubereiten und anzuzeigen
 * @param el
 */
function setAnswerRecord(el)
{
    // Empfaenger setzen
    let userid = parseInt($(el).data('userid'));
    let selectLawyer = $('#select_lawyer');

    if (userid > 0 && selectLawyer.length) {
        selectLawyer.val(userid);
    }

    // Fall setzen
    let caseId = parseInt($(el).data('caseid'));
    let selectCase = $('#select_case');

    if (caseId > 0 && selectCase.length) {
        selectCase.val(caseId);
    }

    // Betreff setzen
    let subjectText = $(el).data('subject');
    let subjectField = $('#message_subject');

    if (subjectText && subjectField.length) {
        console.log(subjectField);
        $(subjectField).val(subjectText);
    }
}

/**
 * das "neue Nachrichten"-Formular anzeigen
 * @returns {boolean}
 */
function showNewRecord()
{
    let newRecord = $('.new_record');

    if (!newRecord.length) {
        return false;
    }

    newRecord.slideDown();
    setCookie('LCP_NEW_RECORD', 1, 1);

    $('html, body').animate({
        scrollTop: newRecord.offset().top
    }, 800, function (){});
}

/**
 * fuer Akkordeon-Effekt: alle offenen Details ausblenden
 */
function closeAllRecordDetails()
{
    //alle schließen
    $('.item_list').find('tr').each(function (i) {

        if ($(this).hasClass('active')) {
            let openBtn = $(this).find('.open_details');
            $(openBtn).removeClass('active');
            $(openBtn).find('span').text($(openBtn).data('opentext'));
            $(this).removeClass('active');
        }

        if ($(this).hasClass('details')) {
            $(this).find('.detail_wrapper').slideUp('fast');
            $(this).hide();
        }
    });
}

/**
 * "neue Nachricht" Formular einklappen
 */
function hideNewRecord()
{
    let newRecord = $('.new_record');
    if (newRecord.length) {
        newRecord.slideUp();
        setCookie('LCP_NEW_RECORD', null, -1);
    }
}

/**
 * wenn die Listen-Reihe geklickt wird Aktionen wie öffnen und als gelesen markieren oder zuklappen lassen
 * @returns {boolean}
 */
function initItemDetails()
{
    let clickFileds = $('td.togglerCol');


    if (clickFileds.length < 1) {
        return false;
    }

    clickFileds.on('click', function (e) {
        e.preventDefault();

        let listRow = $(this).parents('tr.list_entry')[0];
        let openerLink = $(listRow).find('.open_details');
        let recordId = $(openerLink).data('itemid');
        let detailsRowClass = $(openerLink).data('details');
        let details_row = $('.'+detailsRowClass);
        let current_row = $(listRow);
        let gongTotal = $('#header .new_gong .total');

        if ($(current_row).hasClass('active')) {
            $(current_row).removeClass('active');
            $(details_row).find('.detail_wrapper').slideUp('fast', function (e) {
                $(details_row).hide();
            });
            $(openerLink).removeClass('active');
            $(openerLink).find('span').text($(openerLink).data('opentext'));
        } else {
            closeAllRecordDetails();
            $(current_row).addClass('active');
            $(details_row).show();
            $(details_row).find('.detail_wrapper').slideDown('fast');
            $(openerLink).addClass('active');
            $(openerLink).find('span').text($(openerLink).data('closetext'));
        }

        if ($(current_row).hasClass('new')) {
            $(current_row).removeClass('new');

            // in der DB als gelesen setzen
            let redirect = $(current_row).parents('form.form').attr('action');

            if (redirect.length) {
                $.post(redirect, { rid: recordId})
                    .done(function (data) {

                        // Gong-Anzahl im Header reduzieren
                        if (gongTotal.length) {
                            let anzahl =  parseInt($(gongTotal).text());
                            gongTotal.text(anzahl-1);
                        }

                    });
            }
        }
    });
}

/**
 * für "zur Akte" Button, wenn ein Anker gesetzt wurde,
 * auf der "meine Akten" Seite den zutreffenden Detail-Bereich ausklappen
 * @returns {boolean}
 */
function openDetailsByHash()
{
    let hash = $(location).attr('hash');
    if (!hash.length) {
        return false;
    }

    let itemRow = $(hash);
    if (!itemRow.length) {
        return false;
    }

    let openLink = $(itemRow).find('.open_details');
    if (openLink.length) {
        $(openLink).trigger("click");
    }
}

jQuery(document).ready(function ($) {
    initNewRecordContainer();
    initNewRecordBtn();
    initNewRecordCloseBtn();
    initMultiActions();
    initItemDetails();
    initSortingField();
    initCheckall();
    initItemDeleteBtn();
    initDropDraftAndNewBtn();
    initCaseDownloadBtn();
    openDetailsByHash();
});