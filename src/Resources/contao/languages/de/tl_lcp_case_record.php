<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

/*
 * Buttons.
 */
$GLOBALS['TL_LANG']['tl_lcp_case_record']['new'][0] = 'Neue Nachricht';
$GLOBALS['TL_LANG']['tl_lcp_case_record']['new'][1] = 'Eine neue Nachricht anlegen.';
$GLOBALS['TL_LANG']['tl_lcp_case_record']['edit'][0] = 'Nachricht bearbeiten';
$GLOBALS['TL_LANG']['tl_lcp_case_record']['edit'][1] = 'Nachricht ID %s bearbeiten.';
$GLOBALS['TL_LANG']['tl_lcp_case_record']['copy'][0] = 'Nachricht duplizieren';
$GLOBALS['TL_LANG']['tl_lcp_case_record']['copy'][1] = 'Nachricht ID %s duplizieren.';
$GLOBALS['TL_LANG']['tl_lcp_case_record']['delete'][0] = 'Nachricht löschen';
$GLOBALS['TL_LANG']['tl_lcp_case_record']['delete'][1] = 'Nachricht ID %s löschen.';
$GLOBALS['TL_LANG']['tl_lcp_case_record']['show'][0] = 'Nachricht anzeigen';
$GLOBALS['TL_LANG']['tl_lcp_case_record']['show'][1] = 'Nachricht anzeigen (ID: %s)';
$GLOBALS['TL_LANG']['tl_lcp_case_record']['case_records'][0] = 'Alle Nachrichten zur Akte';
$GLOBALS['TL_LANG']['tl_lcp_case_record']['case_records'][1] = 'Alle Nachrichten zur Akte anzeigen.';
$GLOBALS['TL_LANG']['tl_lcp_case_record']['multiEdit'][0] = 'mehrere bearbeiten';

/*
 * Fields
 */
$GLOBALS['TL_LANG']['tl_lcp_case_record']['case'] = ['Akte'];
$GLOBALS['TL_LANG']['tl_lcp_case_record']['tstamp'] = ['bearbeitet am'];
$GLOBALS['TL_LANG']['tl_lcp_case_record']['dateAdded'] = ['erstellt am'];
$GLOBALS['TL_LANG']['tl_lcp_case_record']['dateSend'] = [
    'gesendet am',
    'Das Datum wann eine Nachricht versendet wurde',
];
$GLOBALS['TL_LANG']['tl_lcp_case_record']['dateOpen'] = [
    'geöffnet am',
    'Das Datum wann eine Nachricht geöffnet wurde',
];
$GLOBALS['TL_LANG']['tl_lcp_case_record']['authorId'] = [
    'Nachrichten-Verfasser',
    'Der zur Nachricht gehörende Verfasser wird automatisch gesetzt.',
];
$GLOBALS['TL_LANG']['tl_lcp_case_record']['authorName'] = [
    'Name des Verfassers',
    'Der zur Nachricht gehörende Verfasser-Name wird automatisch gesetzt.',
];
$GLOBALS['TL_LANG']['tl_lcp_case_record']['sendToEmail'] = [
    'Adressat',
    'Der Nachrichten Empfänger wird hier angegeben.',
];
$GLOBALS['TL_LANG']['tl_lcp_case_record']['authorTable'] = [
    'Tabelle des Verfassers',
    'Der zur Nachricht gehörende Verfasser-Tabelle wird automatisch gesetzt. (tl_user->Anwalt, tl_member->Mandant)',
];

$GLOBALS['TL_LANG']['tl_lcp_case_record']['userId'] = [
    'Anwalt',
    'Wählen Sie den zur Nachricht gehörenden Anwalt aus.',
];

$GLOBALS['TL_LANG']['tl_lcp_case_record']['memberId'] = [
    'Mandant',
    'Wählen Sie den zur Nachricht gehörenden Mandant aus.',
];

$GLOBALS['TL_LANG']['tl_lcp_case_record']['subject'] = [
    'Betreff',
    'Geben Sie hier ähnlich einer Email den Betreff der Nachricht ein.',
];
$GLOBALS['TL_LANG']['tl_lcp_case_record']['text'][0] = 'Nachricht';
$GLOBALS['TL_LANG']['tl_lcp_case_record']['upload'] = [
    'Datei-Upload',
    'Hier können Sie ein oder mehrere Dateien hochladen so das diese dann zu der Nachricht mitgesendet werden.',
];

$GLOBALS['TL_LANG']['tl_lcp_case_record']['sendBySave'] = [
    'sofort senden',
    'Aktivieren Sie dieses Kästchen, wenn Sie die Nachricht sofort versenden wollen.',
];
$GLOBALS['TL_LANG']['tl_lcp_case_record']['readyToSend'] = [
    'Versand planen',
    'Aktivieren Sie dieses Kästchen, wenn Sie den Nachrichtenversand planen wollen. 
    Sie können dann eine Uhrzeit festlegen.',
];
$GLOBALS['TL_LANG']['tl_lcp_case_record']['sendByDate'] = ['Versanddatum und -zeit'];
$GLOBALS['TL_LANG']['tl_lcp_case_record']['upload_button_text'] = 'Datei(en) hochladen';
$GLOBALS['TL_LANG']['tl_lcp_case_record']['new'] = ['neue Nachricht'];
$GLOBALS['TL_LANG']['tl_lcp_case_record']['deleted'] = ['gelöschte Nachricht'];
/*
 * Legends
 */
$GLOBALS['TL_LANG']['tl_lcp_case_record']['main_legend'] = 'Haupt-Einstellungen';
$GLOBALS['TL_LANG']['tl_lcp_case_record']['message_legend'] = 'Nachrichten-Einstellungen';
$GLOBALS['TL_LANG']['tl_lcp_case_record']['upload_legend'] = 'Datei-Einstellungen';
$GLOBALS['TL_LANG']['tl_lcp_case_record']['published_legend'] = 'Versand-Einstellungen';
$GLOBALS['TL_LANG']['tl_lcp_case_record']['member_option_legend'] = 'vom Mandant gesetzte Werte';
$GLOBALS['TL_LANG']['tl_lcp_case_record']['extend_legend'] = 'weitere Einstellungen';

/*
 * Meldungen
 */
$GLOBALS['TL_LANG']['tl_lcp_case_record']['delete_files_success']
    = 'Es wurden nicht verwendete Dateien aus dem Aktenordner gelöscht.';
$GLOBALS['TL_LANG']['tl_lcp_case_record']['no_entries_msg'] = 'Es sind noch keine Nachrichten vorhanden.';
