<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

/*
 * Fields.
 */
$GLOBALS['TL_LANG']['tl_lcp_case']['title'] = ['Aktenbezeichnung'];
$GLOBALS['TL_LANG']['tl_lcp_case']['tstamp'] = ['Bearbeitet am'];
$GLOBALS['TL_LANG']['tl_lcp_case']['dateAdded'] = ['Erstellt am'];
$GLOBALS['TL_LANG']['tl_lcp_case']['createdFrom'] = ['erstellt von'];
$GLOBALS['TL_LANG']['tl_lcp_case']['caseNumber'] = [
    'Aktenzeichen',
    'Geben Sie hier das Aktenzeichen ein.',
];
$GLOBALS['TL_LANG']['tl_lcp_case']['memberId'] = [
    'Mandant',
    'Wählen Sie den zur Akte gehörenden Mandanten aus.',
];
$GLOBALS['TL_LANG']['tl_lcp_case']['userId'] = [
    'Anwalt',
    'Wählen Sie den zur Akte gehörenden Anwalt aus.',
];

$GLOBALS['TL_LANG']['tl_lcp_case']['start'] = [
    'Akte geöffnet am',
    'Geben Sie hier an wann die Akte angelegt wurde.',
];
$GLOBALS['TL_LANG']['tl_lcp_case']['stop'] = [
    'Akte geschlossen am',
    'Geben Sie hier an wann die Akte geschlossen wurde.',
];
$GLOBALS['TL_LANG']['tl_lcp_case']['published'] = ['Akte aktiv'];
$GLOBALS['TL_LANG']['tl_lcp_case']['notice'] = ['Notiz'];
$GLOBALS['TL_LANG']['tl_lcp_case']['caseFolder'] = [
    'Archiv-Speicherort',
    'Wird beim anlegen einer Akte automatisch durch den Speicherort aus den 
    Einstellungen und dem Aktenzeichen erstellt und hier gespeichert.',
];

/*
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_lcp_case']['new'][0] = 'Neue Akte';
$GLOBALS['TL_LANG']['tl_lcp_case']['new'][1] = 'Eine neue Akte anlegen.';
$GLOBALS['TL_LANG']['tl_lcp_case']['edit'][0] = 'Akte bearbeiten';
$GLOBALS['TL_LANG']['tl_lcp_case']['edit'][1] = 'Akte ID %s bearbeiten.';
$GLOBALS['TL_LANG']['tl_lcp_case']['records'][0] = 'Nachrichten anzeigen';
$GLOBALS['TL_LANG']['tl_lcp_case']['records'][1] = 'Nachrichten dieser Akte anzeigen.';
$GLOBALS['TL_LANG']['tl_lcp_case']['compress'][0] = 'Zip-Datei erzeugen';
$GLOBALS['TL_LANG']['tl_lcp_case']['compress'][1] = 'Die Nachrichten und Dateien zur Akte als Zip-Datei runterladen.';
$GLOBALS['TL_LANG']['tl_lcp_case']['delete'][0] = 'Akte löschen';
$GLOBALS['TL_LANG']['tl_lcp_case']['delete'][1] = 'Akte ID %s löschen.';
$GLOBALS['TL_LANG']['tl_lcp_case']['multiEdit'][0] = 'mehrere bearbeiten';

/*
 * Legends
 */
$GLOBALS['TL_LANG']['tl_lcp_case']['meta_legend'] = 'Grund-Informationen';
$GLOBALS['TL_LANG']['tl_lcp_case']['member_user_legend'] = 'Mandant & Anwalt -Zuweisung';
$GLOBALS['TL_LANG']['tl_lcp_case']['notice_legend'] = 'Notiz-Einstellungen';
$GLOBALS['TL_LANG']['tl_lcp_case']['published_legend'] = 'Veröffentlichungs-Einstellungen';
$GLOBALS['TL_LANG']['tl_lcp_case']['expert_legend'] = 'Experten-Einstellungen';

/*
 * Messages
 */
$GLOBALS['TL_LANG']['tl_lcp_case']['zip_not_exist'] =
    'Zip-Datei  "%s" existiert nicht. Evtl sind keine Nachrichten und Dateien enthalten.';
$GLOBALS['TL_LANG']['tl_lcp_case']['not_export_notification_isset'] =
    'Es wurde in den Einstellungen keine Notification für den Archiv-Export zugewiesen.';
$GLOBALS['TL_LANG']['tl_lcp_case']['notification_send_error'] =
    'Es sind Fehler beim Versand der notification aufgetreten.';
$GLOBALS['TL_LANG']['tl_lcp_case']['notification_send_success'] = 'Die Notification wurde erfolgreich versendet.';
$GLOBALS['TL_LANG']['tl_lcp_case']['no_entries_msg'] = 'Es sind keine Akten vorhanden.';
