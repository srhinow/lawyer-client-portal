<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

$GLOBALS['TL_LANG']['tl_nc_notification']['type']['lcp'] = 'Layer Client Portal';
$GLOBALS['TL_LANG']['tl_nc_notification']['type']['lcp_send_archives'] = [
    'Archiv-Export',
    'mit dieser E-Mail werden alle markierten Archive an die eingerichtete E-Mail versand.',
];
