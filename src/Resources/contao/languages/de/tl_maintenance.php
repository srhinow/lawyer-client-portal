<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

$GLOBALS['TL_LANG']['tl_maintenance_jobs']['lcp_clean_exports'] = [
    'Export-Daten bereinigen',
    'Löscht alle zum Export erzeugten Zip-Dateien und record-TXT Dateien.',
];

$GLOBALS['TL_LANG']['tl_maintenance_jobs']['lcp_clean_uploads'] = [
    'Upload-Daten bereinigen',
    'Löscht alle zum Fall-Nachrichten durch Uploads erzeugten Dateien.',
];
