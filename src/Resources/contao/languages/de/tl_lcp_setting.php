<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

/*
 * Fields.
 */
$GLOBALS['TL_LANG']['tl_lcp_setting']['userGroupId'] = [
    'Verwaltungs-Gruppe',
    'Weisen Sie hier die Gruppe zu, welche die Fälle im Mandantenportal bearbeiten wird.',
];
$GLOBALS['TL_LANG']['tl_lcp_setting']['memberGroupId'] = [
    'Mandantengruppe',
    'Legen sie hier fest welcher Gruppe die Mandanten im Portal angehören.',
];
$GLOBALS['TL_LANG']['tl_lcp_setting']['placeForCases'] = [
    'Akten-Speicherort',
    'Legen sie hier fest wo im System die Archivordner mit allen zugehörenden Dateien abgelegt werden.',
];
$GLOBALS['TL_LANG']['tl_lcp_setting']['tmpFolder'] = [
    'Temp-Speicherort',
    'Legen sie hier fest wo im System die Temporär für den Export erzeugten Zip-Dateien 
    und Record-TXT-Dateien abgelegt werden.',
];
$GLOBALS['TL_LANG']['tl_lcp_setting']['uploadFolder'] = [
    'Upload-Speicherort',
    'Legen sie hier fest wo im System der  Upload-Ordner für die hochgeladenen Dateien abgelegt werden.',
];
$GLOBALS['TL_LANG']['tl_lcp_setting']['activateNotification'] = [
    'Aktivierungs-E-Mail',
    'Legen sie hier fest welche Aktivierungs-E-Mail versendet werden soll.',
];
$GLOBALS['TL_LANG']['tl_lcp_setting']['sendTokenNotification'] = [
    'Mandant-Token-E-Mail',
    'Legen sie hier fest welche E-Mail versendet werden soll, 
    damit der Mandant sein Passwort setzen kann damit er sich zukünftig anmelden kann.',
];
$GLOBALS['TL_LANG']['tl_lcp_setting']['newRecordNotification'] = [
    'neue Nachricht E-Mail',
    'Legen sie hier fest welche E-Mail an den Mandanten versendet werden soll, die auf neue Nachrichten hinweist.',
];
$GLOBALS['TL_LANG']['tl_lcp_setting']['reg_jumpTo'] = [
    'Zugang-Passwort-setzen - Seite',
    'Legen sie hier fest welche URL das System für die Token-Url nutzen soll, 
    damit der Mandant sein Passwort für den geschützten Zugang setzen kann.',
];
$GLOBALS['TL_LANG']['tl_lcp_setting']['cronTime'] = [
    'Cron-Zeit',
    'Geben Sie hier die Zeit ein ab wann der nächste Cron ausgeführt werden soll. (z.B. 16:00)',
];
$GLOBALS['TL_LANG']['tl_lcp_setting']['closedAfterDays'] = [
    'Akte schließen nach x Tagen',
    'Geben Sie hier die Tage an, ab wann die Akten deaktiviert werden soll.',
];
$GLOBALS['TL_LANG']['tl_lcp_setting']['uploadFileExtensions'] = [
    'erlaubte Datei-Erweiterungen',
    'Kommagetrennt mit Punkt z.B. .gif,.pdf,.odt,.docx',
];
$GLOBALS['TL_LANG']['tl_lcp_setting']['uploadFileSize'] = [
    'erlaubte Datei-Größe',
    'Wird nur als Zahl in MB angegeben daher muss z.B 5 eingegeben werden.',
];
$GLOBALS['TL_LANG']['tl_lcp_setting']['uploadMaxFiles'] = [
    'max. Anzahl an Dateien',
    'Wird nur als Zahl angegeben daher muss z.B 5 eingegeben werden.',
];
$GLOBALS['TL_LANG']['tl_lcp_setting']['beforeUploadText'] = [
    'Text vor dem Upload-Feld',
    'Der Text der im "neue Nachricht" Bereich vor dem Upoadfeld dargestellt wird.',
];

/*
 * Legends
 */
$GLOBALS['TL_LANG']['tl_lcp_setting']['groups_legend'] = 'Gruppen-Einstellungen';
$GLOBALS['TL_LANG']['tl_lcp_setting']['folder_legend'] = 'Pfad-Einstellungen';
$GLOBALS['TL_LANG']['tl_lcp_setting']['notification_legend'] = 'Benachrichtigungen';
$GLOBALS['TL_LANG']['tl_lcp_setting']['upload_legend'] = 'Upload-Einstellungen';
$GLOBALS['TL_LANG']['tl_lcp_setting']['cron_legend'] = 'Cron-Einstellungen';
