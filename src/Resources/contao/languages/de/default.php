<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

$GLOBALS['TL_LANG']['MSC']['edit'] = 'Bearbeiten';
$GLOBALS['TL_LANG']['MSC']['copy'] = 'Kopieren';
$GLOBALS['TL_LANG']['MSC']['createZips'] = 'Fälle als Zips exportieren';
