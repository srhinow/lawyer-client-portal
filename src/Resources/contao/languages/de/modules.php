<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

/*
 * Back end modules.
 */
$GLOBALS['TL_LANG']['MOD']['lawyerclientportal'] = ['Mandanten-Portal', 'Das Rechtsanwalts-Mandanten-Portal'];
$GLOBALS['TL_LANG']['MOD']['lcp_case'] = ['Akten', 'Liste aller laufenden Akten.'];
$GLOBALS['TL_LANG']['MOD']['lcp_inbox'] = ['Posteingang', 'Verwaltung aller eingegangenen Nachrichten.'];
$GLOBALS['TL_LANG']['MOD']['lcp_draft'] = ['Entwürfe', 'Verwaltung aller Entwürfe.'];
$GLOBALS['TL_LANG']['MOD']['lcp_outbox'] = ['Postausgang', 'Verwaltung aller zum Versand anstehenden Nachrichten.'];
$GLOBALS['TL_LANG']['MOD']['lcp_clients'] = ['Mandanten', 'Verwaltung aller Mandanten.'];
$GLOBALS['TL_LANG']['MOD']['lcp_setting'] = ['Einstellungen', 'Mandanten-Portal-Einstellungen'];

/*
 * Front end modules.
 */
$GLOBALS['TL_LANG']['FMD']['lawyerclientportal'] = 'Lawyer Client Portal';
$GLOBALS['TL_LANG']['FMD']['lcp_fe_case_list'] = ['LCP :: Akten-Liste'];
$GLOBALS['TL_LANG']['FMD']['lcp_fe_header_new_record'] = ['LCP :: neue Nachrichten (Kopfbereich)'];
$GLOBALS['TL_LANG']['FMD']['lcp_fe_lawyer_teaser'] = ['LCP :: Anwahlt-Teaser'];
$GLOBALS['TL_LANG']['FMD']['lcp_fe_record_list'] = ['LCP :: Nachrichten'];
$GLOBALS['TL_LANG']['FMD']['lcp_fe_set_password'] = ['LCP :: Passwort setzen'];
$GLOBALS['TL_LANG']['FMD']['lcp_fe_set_username'] = ['LCP :: Benutzername und E-Mail setzen'];

// ??? weiß nicht mehr wofuer das war ???
$GLOBALS['TL_LANG']['FE_MOD']['lcp_case'] = ['Fälle', 'Die Fall-Verwaltung'];
$GLOBALS['TL_LANG']['FE_MOD']['lcp_member_dashboard'] = [
    'Mandanten Dashboard',
    'Ausgabe einer Übersicht auf der intern geschützten Startseite.',
];
