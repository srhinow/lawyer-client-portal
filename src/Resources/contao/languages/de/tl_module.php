<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

// Fields
$GLOBALS['TL_LANG']['tl_module']['lcp_fe_template'] = ['LCP-Template', ''];
$GLOBALS['TL_LANG']['tl_module']['lcp_numberOfItems'] = ['Anzahl der Einträge', ''];
$GLOBALS['TL_LANG']['tl_module']['jumpToCaseList'] = ['zur Aktenliste', ''];
$GLOBALS['TL_LANG']['tl_module']['jumpToRecordList'] = ['zur Nachrichtenliste', ''];
