<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

$GLOBALS['TL_LANG']['tl_user_group']['lcpcaseportal_legend'] = 'Mandantenportal-Rechte';

$GLOBALS['TL_LANG']['tl_user_group']['lcp_casep'] = ['Fallrechte'];
$GLOBALS['TL_LANG']['tl_user_group']['lcp_case_recordp'] = ['Fall-Nachrichten-Rechte'];
