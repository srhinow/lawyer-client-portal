<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

/*
 * Fields.
 */
$GLOBALS['TL_LANG']['tl_member']['salution'] = ['Anrede'];
$GLOBALS['TL_LANG']['tl_member']['sid'] = [
    'Sicherheits-ID',
    'Wird für den ersten Login-Link benötigt.',
];
$GLOBALS['TL_LANG']['tl_member']['sendLogin'] = [
    'Die Login-E-Mail senden',
    'Wenn die Checkbox gewählt ist, wird eine E-Mail mit Anmeldelink an den Mandanten gesendet.',
];

$GLOBALS['TL_LANG']['tl_member']['activation_legend'] = 'Aktivierungs-Einstellungen';
