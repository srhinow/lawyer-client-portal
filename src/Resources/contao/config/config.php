<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

$GLOBALS['LAWYERCLIENTPORTAL_SETTINGS']['ID'] = 1;
$GLOBALS['LAWYERCLIENTPORTAL_PUBLIC_FOLDER'] = 'bundles/srhinowlawyerclientportal';
$GLOBALS['LAWYERCLIENTPORTAL_RECORD_FOLDER'] = 'records';
$GLOBALS['LAWYERCLIENTPORTAL_MANDANT_DEFAULT_PW'] = 'flmanp0rt';
/*
 * -------------------------------------------------------------------------
 * BACK END MODULES
 * -------------------------------------------------------------------------
 */
if ('BE' === TL_MODE) {
    $GLOBALS['TL_CSS'][] = $GLOBALS['LAWYERCLIENTPORTAL_PUBLIC_FOLDER'].'/css/be.css|static';
}

array_insert($GLOBALS['BE_MOD'], 1, ['lawyerclientportal' => []]);

$GLOBALS['BE_MOD']['lawyerclientportal']['lcp_case'] = [
    'tables' => ['tl_lcp_case', 'tl_lcp_case_record'],
    'compress' => ['srhinow.lawyer_client_portal.listener.dca.lcp_case', 'getCaseAsZip'],
    'readMessage' => ['srhinow.lawyer_client_portal.listener.dca.lcp_case_record', 'readMessage'],
    'stylesheet' => [$GLOBALS['LAWYERCLIENTPORTAL_PUBLIC_FOLDER'].'/css/case.css'],
];

$GLOBALS['BE_MOD']['lawyerclientportal']['lcp_inbox'] = [
    'tables' => ['tl_lcp_case_record', 'tl_lcp_case'],
    'readMessage' => ['srhinow.lawyer_client_portal.listener.dca.lcp_case_record', 'readMessage'],
];

$GLOBALS['BE_MOD']['lawyerclientportal']['lcp_draft'] = [
    'tables' => ['tl_lcp_case_record', 'tl_lcp_case'],
    'readMessage' => ['srhinow.lawyer_client_portal.listener.dca.lcp_case_record', 'readMessage'],
];

$GLOBALS['BE_MOD']['lawyerclientportal']['lcp_outbox'] = [
    'tables' => ['tl_lcp_case_record', 'tl_lcp_case'],
    'readMessage' => ['srhinow.lawyer_client_portal.listener.dca.lcp_case_record', 'readMessage'],
];

$GLOBALS['BE_MOD']['lawyerclientportal']['lcp_clients'] = [
    'tables' => ['tl_member'],
    'callback' => 'Srhinow\LawyerClientPortal\Modules\Backend\ModuleCustomerMember',
    'icon' => $GLOBALS['LAWYERCLIENTPORTAL_PUBLIC_FOLDER'].'/icons/users.png',
];

$GLOBALS['BE_MOD']['lawyerclientportal']['lcp_setting'] = [
    'tables' => ['tl_lcp_setting'],
    'callback' => 'Srhinow\LawyerClientPortal\Modules\Backend\ModulePortalSettings',
];

/*
 * -------------------------------------------------------------------------
 * Front END MODULES
 * -------------------------------------------------------------------------
 */
array_insert(
    $GLOBALS['FE_MOD'],
    2,
    [
        'lawyerclientportal' => [
            'lcp_fe_case_list' => 'Srhinow\LawyerClientPortal\Modules\Frontend\ModuleCaseList',
            'lcp_fe_header_new_record' => 'Srhinow\LawyerClientPortal\Modules\Frontend\ModuleHeaderNewRecords',
            'lcp_fe_lawyer_teaser' => 'Srhinow\LawyerClientPortal\Modules\Frontend\ModuleRelevantLawyerTeaser',
            'lcp_fe_record_list' => 'Srhinow\LawyerClientPortal\Modules\Frontend\ModuleCaseRecordList',
            'lcp_fe_set_password' => 'Srhinow\LawyerClientPortal\Modules\Frontend\ModuleLcpPassword',
            'lcp_fe_set_username' => 'Srhinow\LawyerClientPortal\Modules\Frontend\ModuleLcpChangeEmailAndUsername',
        ],
    ]
);

/*
 * -------------------------------------------------------------------------
 * MODELS
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_MODELS']['tl_lcp_setting'] = Srhinow\LawyerClientPortal\Model\LcpSettingModel::class;
$GLOBALS['TL_MODELS']['tl_lcp_case'] = Srhinow\LawyerClientPortal\Model\LcpCaseModel::class;
$GLOBALS['TL_MODELS']['tl_lcp_case_record'] = Srhinow\LawyerClientPortal\Model\LcpCaseRecordModel::class;

/*
 * -------------------------------------------------------------------------
 * HOOKS
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_HOOKS']['getSystemMessages'][] = [
    'srhinow.lawyer_client_portal.listener.hook.get_system_messages',
    'onGetSystemMessages',
];

/*
 * -------------------------------------------------------------------------
 * Permissions are access settings for user and groups (fields in tl_user and tl_user_group)
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_PERMISSIONS'][] = 'lcp_casep';
$GLOBALS['TL_PERMISSIONS'][] = 'lcp_case_recordp';

/*
 * -------------------------------------------------------------------------
 * Bereitgesteller Notification-Typ und dafuer verfuegbare Tokens
 * -------------------------------------------------------------------------
 */
$GLOBALS['NOTIFICATION_CENTER']['NOTIFICATION_TYPE']['lcp'] = [
    // Type
    'lcp_send_archives' => [
        // Field in tl_nc_language
        'email_sender_name' => ['email_sender_name'],
        'email_sender_address' => ['email_sender_email'],
        'recipients' => ['send_to'],
        'email_replyTo' => ['reply_to'],
        'email_recipient_cc' => ['recipient_cc'],
        'email_recipient_bcc' => ['recipient_bcc'],
        'email_subject' => ['email_subject', 'case_number'],
        'email_text' => ['email_text', 'case_number'],
        'email_html' => ['email_html', 'case_number'],
        'attachment_tokens' => [
            'zip_archives', // The document that should be attached (e.g. an invoice)
        ],
    ],
];

/*
 * -------------------------------------------------------------------------
 * Maintenance
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_PURGE']['custom']['lcp_clean_exports'] = [
    'callback' => ['Srhinow\\LawyerClientPortal\\Automator\\LcpAutomator', 'cleanCaseTempFolder'],
];
$GLOBALS['TL_PURGE']['custom']['lcp_clean_uploads'] = [
    'callback' => ['Srhinow\\LawyerClientPortal\\Automator\\LcpAutomator', 'cleanUploadFolder'],
];
