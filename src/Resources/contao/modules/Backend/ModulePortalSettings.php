<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\Modules\Backend;

use Contao\BackendModule;

class ModulePortalSettings extends BackendModule
{
    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'mod_properties';

    /**
     * Change the palette of the current table and switch to edit mode.
     */
    public function generate()
    {
        return $this->objDc->edit($GLOBALS['LAWYERCLIENTPORTAL_SETTINGS']['ID']);
    }

    /**
     * Generate module.
     */
    protected function compile()
    {
        return '';
    }
}
