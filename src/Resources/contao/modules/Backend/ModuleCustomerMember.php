<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\Modules\Backend;

use Contao\Backend;
use Contao\BackendModule;
use Srhinow\LawyerClientPortal\Model\LcpSettingModel;

class ModuleCustomerMember extends BackendModule
{
    protected $settings = [];

    /**
     * Change the palette of the current table and switch to edit mode.
     */
    public function generate()
    {
        if ('tl_member' !== $this->table) {
            Backend::redirect(str_replace('do=lcp_client', 'do=member', \Environment::get('request')));
        }

        //Einstellungen holen
        $this->settings = LcpSettingModel::findByPk($GLOBALS['LAWYERCLIENTPORTAL_SETTINGS']['ID']);
        if (null === $this->settings) {
            Backend::redirect(str_replace('do=lcp_clients', 'do=member', \Environment::get('request')));
        }

        $GLOBALS['TL_DCA'][$this->table]['palettes'] = [
            '__selector__' => $GLOBALS['TL_DCA'][$this->table]['palettes']['__selector__'],
            'default' => $GLOBALS['TL_DCA'][$this->table]['palettes']['lcp_clients'],
        ];

        $GLOBALS['TL_DCA'][$this->table]['list']['sorting'] = [
            'flag' => 11,
            'panelLayout' => 'filter;sort,search,limit',
            'filter' => [['`tl_member`.`groups` LIKE \'%"'.$this->settings->memberGroupId.'"%\'', []]],
        ];
        $GLOBALS['TL_DCA'][$this->table]['list']['label']['fields']
            = ['icon', 'salution', 'firstname', 'lastname', 'email', 'dateAdded'];

        $GLOBALS['TL_DCA'][$this->table]['fields']['login']['default'] = 1;
        $GLOBALS['TL_DCA'][$this->table]['fields']['password']['default']
            = $GLOBALS['LAWYERCLIENTPORTAL_MANDANT_DEFAULT_PW'];

        $GLOBALS['TL_DCA'][$this->table]['fields']['groups']['default'] = [$this->settings->memberGroupId];

        unset($GLOBALS['TL_DCA'][$this->table]['list']['operations']['addresses']);
        $act = \Input::get('act');

        return (!$act || 'select' === $act) ? $this->objDc->showAll() : $this->objDc->{$act}();
    }

    /**
     * Generate module.
     */
    protected function compile()
    {
        return '';
    }
}
