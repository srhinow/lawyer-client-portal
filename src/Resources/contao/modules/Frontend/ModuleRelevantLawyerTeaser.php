<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\Modules\Frontend;

use Contao\BackendTemplate;
use Contao\FrontendUser;
use Contao\Module;
use Contao\UserModel;
use Srhinow\LawyerClientPortal\Model\LcpCaseModel;

class ModuleRelevantLawyerTeaser extends Module
{
    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'lcp_lawyer_teaser';

    /**
     * Target pages.
     *
     * @var array
     */
    protected $arrTargets = [];

    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE === 'BE') {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### ANWÄLTE-TEASER ###';

            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=modules&amp;act=edit&amp;id='.$this->id;

            return $objTemplate->parse();
        }

        // Fallback template
        if ($this->lcp_fe_template) {
            $this->strTemplate = $this->lcp_fe_template;
        }

        return parent::generate();
    }

    /**
     * Generate module.
     */
    protected function compile(): void
    {
        if (!FE_USER_LOGGED_IN) {
            return;
        }

        $Member = FrontendUser::getInstance();

        $objCases = LcpCaseModel::findPublishedByMember($Member->id, 0, 0, ['group' => 'userId']);
        if (null === $objCases) {
            return;
        }

        $arrUser = [];
        while ($objCases->next()) {
            $arrUser[] = $objCases->userId;
        }

        $objUser = UserModel::findMultipleByIds($arrUser);
        if (null === $objUser) {
            return;
        }

        $arrContentTags = [];
        while ($objUser->next()) {
            $arrContentTags[] = '{{insert_content::'.$objUser->contentTeaser.'}}';
        }

        $this->Template->headline = $this->headline;
        $this->Template->content = implode('', $arrContentTags);
    }
}
