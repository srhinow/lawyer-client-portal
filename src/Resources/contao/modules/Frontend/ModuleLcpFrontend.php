<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\Modules\Frontend;

use Contao\Module;
use Contao\Pagination;

abstract class ModuleLcpFrontend extends Module
{
    public $offset = 0;

    public $limit;

    public $total = 0;

    public function setListPagination(): void
    {
        // Maximum number of items
        if ($this->lcp_numberOfItems > 0) {
            $this->limit = $this->lcp_numberOfItems;
        }

        // Split the results
        if ($this->perPage > 0 && (!isset($this->limit) || $this->fe_iao_numberOfItems > $this->perPage)) {
            // Adjust the overall limit
            if (isset($this->limit)) {
                $this->total = min($this->limit, $this->total);
            }

            // Get the current page
            $page = \Input::get('page') ?: 1;

            // Do not index or cache the page if the page number is outside the range
            if ($page < 1 || $page > max(ceil($this->total / $this->perPage), 1)) {
                global $objPage;
                $objPage->noSearch = 1;
                $objPage->cache = 0;

                // Send a 404 header
                header('HTTP/1.1 404 Not Found');

                return;
            }

            // Set limit and offset
            $this->limit = $this->perPage;
            $this->offset = (max($page, 1) - 1) * $this->perPage;

            // Overall limit
            if ($this->offset + $this->limit > $this->total) {
                $this->limit = $this->total - $this->offset;
            }

            // Add the pagination menu
            $objPagination = new Pagination($this->total, $this->perPage);
            $this->Template->pagination = $objPagination->generate("\n  ");
        }
    }
}
class_alias(ModuleLcpFrontend::class, 'ModuleLcpFrontend');
