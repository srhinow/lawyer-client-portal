<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\Modules\Frontend;

use Contao\BackendTemplate;
use Contao\Controller;
use Contao\FilesModel;
use Contao\FrontendUser;
use Contao\Input;
use Contao\PageModel;
use Contao\UserModel;
use Srhinow\LawyerClientPortal\EventListener\Dca\LcpSetting;
use Srhinow\LawyerClientPortal\Helper\CaseRecordHelper;
use Srhinow\LawyerClientPortal\Helper\DropzoneHelper;
use Srhinow\LawyerClientPortal\Model\LcpCaseModel;
use Srhinow\LawyerClientPortal\Model\LcpCaseRecordModel;

class ModuleCaseRecordList extends ModuleLcpFrontend
{
    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'lcp_case_record_list';

    /**
     * Target pages.
     *
     * @var array
     */
    protected $arrTargets = [];

    /**
     * Display a wildcard in the back end.
     *
     * @throws \Exception
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE === 'BE') {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### MANDANT NACHRICHTEN LISTE ###';

            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=modules&amp;act=edit&amp;id='.$this->id;

            return $objTemplate->parse();
        }

        // Ajax Requests abfangen
        if ($this->Environment->get('isAjaxRequest')) {
            if (!empty(Input::get('del'))) {
                $DropZoneHelper = new DropzoneHelper();
                $DropZoneHelper->remove(Input::get('del'));
            } elseif (\is_array($_FILES) && \count($_FILES) > 0) {
                $DropZoneHelper = new DropzoneHelper();
                $DropZoneHelper->upload($_FILES);
            } elseif (Input::post('rid')) {
                $RecordHelper = new CaseRecordHelper();
                $RecordHelper->setRecordAsRead(Input::post('rid'));
            }

            exit();
        }

        // Fallback template
        if ($this->lcp_fe_template) {
            $this->strTemplate = $this->lcp_fe_template;
        }

        // Set the item from the auto_item parameter
        if ($GLOBALS['TL_CONFIG']['useAutoItem'] && \Input::get('auto_item')) {
            \Input::setGet('pid', \Input::get('auto_item'));
        }

        return parent::generate();
    }

    /**
     * Generate module.
     */
    protected function compile(): void
    {
        if (!FE_USER_LOGGED_IN) {
            return;
        }

        $RecordHelper = new CaseRecordHelper();
        $objSettings = LcpSetting::getLcpSettings();
        $Member = FrontendUser::getInstance();
        $this->loadLanguageFile('tl_lcp_case_record');
        $arrItems = $arrProjects = $arrProjIds = [];
        $newRecordFormId = 'new_record_form_'.$this->id;
        $listRecordFormId = 'list_record_form'.$this->id;
        $dbColumns = [];
        $options = [];
        $arrLawyer = [];
        $arrCases = [];
        $dbSearch = (string) Input::post('search');

        if(Input::get('do') === 'showfile') {
            $RecordHelper->showCaseForMemberDocument();
        }

        //CasePage-Url generieren
        $objCasePage = PageModel::findByPk($this->jumpToCaseList);
        $casePagePath = (null === $objCasePage) ? $_SERVER['REQUEST_URI'] : $objCasePage->getFrontendUrl();

        //verarbeiten von "neue Nachrichten" - Events
        if ('new_case_record' === Input::post('FORM_SUBMIT')) {
            //Entwurf löschen
            if (null !== Input::post('dropdraft')) {
                if ($RecordHelper->deleteDraftRecordFormActions()) {
                    setcookie('LCP_MESSAGE', 'Der Entwurf wurde gelöscht.', time() + 1000000);
                    setcookie('LCP_ALERT_TYPE', 'success', time() + 1000000);
                } else {
                    setcookie(
                        'LCP_MESSAGE',
                        'Es ist ein Fehler aufgetreten. Der Entwurf wurde nicht gelöscht.',
                        time() + 1000000
                    );
                    setcookie('LCP_ALERT_TYPE', 'danger', time() + 1000000);
                }
                Controller::reload();
            }

            //Entwurf speichern
            if (null !== Input::post('draft')) {
                if ($RecordHelper->saveNewRecordFormActions()) {
                    setcookie('LCP_MESSAGE', 'Der Entwurf wurde gespeichert.', time() + 1000000);
                    setcookie('LCP_ALERT_TYPE', 'success', time() + 1000000);
                } else {
                    setcookie(
                        'LCP_MESSAGE',
                        'Es ist ein Fehler aufgetreten. Der Entwurf wurde nicht gespeichert.',
                        time() + 1000000
                    );
                    setcookie('LCP_ALERT_TYPE', 'danger', time() + 1000000);
                }
                Controller::reload();
            }

            // Nachricht senden
            if (null !== Input::post('send')) {
                if ($RecordHelper->saveNewRecordFormActions()) {
                    setcookie('LCP_MESSAGE', 'Die Nachricht wurde gesendet.', time() + 1000000);
                    setcookie('LCP_ALERT_TYPE', 'success', time() + 1000000);
                } else {
                    setcookie(
                        'LCP_MESSAGE',
                        'Es ist ein Fehler aufgetreten. Die Nachricht wurde nicht gesendet.',
                        time() + 1000000
                    );
                    setcookie('LCP_ALERT_TYPE', 'danger', time() + 1000000);
                }
                Controller::reload();
            }
        }

        // nach Case-Filter als GET-Paramter schauen und setzen
        if ((int) Input::get('case') > 0) {
            $dbColumns['pid'] = (int) Input::get('case');
        }

        // Get the total number of items
        $this->total = LcpCaseRecordModel::countAllPostedRecordsByMemberId($Member->id, $dbColumns, $dbSearch);

        if ($this->total > 0) {
            $this->setListPagination();

            //falls eine Sortierung übergeben wurde diese setzen
            $sorting = Input::post('sorting');
            if (isset($sorting) && \strlen($sorting) > 0) {
                $options['order'] = $sorting;
            }

            $action = Input::post('action');
            $actionItems = Input::post('items');

            // Aktionen der Liste verarbeiten
            if (\strlen((string) $action) && null !== $actionItems) {
                switch ($action) {
                    case 'read':
                        $RecordHelper->changeReadMultiRecords($actionItems, true);
                        break;

                    case 'unread':
                        $RecordHelper->changeReadMultiRecords($actionItems, false);
                        break;

                    case 'delete':
                        $RecordHelper->deleteMultiRecords($actionItems);
                        break;
                }
            }

            $objRecords = LcpCaseRecordModel::findPostedRecordsByMember(
                $Member->id,
                $dbColumns,
                $dbSearch,
                $this->limit,
                $this->offset,
                $options
            );

            if (null !== $objRecords) {
                while ($objRecords->next()) {
                    //Case-Details holen
                    $objCase = LcpCaseModel::findByPk($objRecords->pid);

                    // Sendername
                    $senderName = 'Sie';
                    $deleteAllowed = true;

                    if ('tl_user' === $objRecords->authorTable) {
                        $objUser = UserModel::findByPk($objRecords->userId);
                        if (null !== $objUser) {
                            // falls vom Anwalt gesendet -> den Sendername ueberschreiben
                            $senderName = (\strlen($objUser->shortName) > 0) ? $objUser->shortName : $objUser->name;

                            // nicht loeschen-Flag setzen
                            $deleteAllowed = false;

                            // fuer neue Nachricht: Anwalt-Selectfeld sammeln
                            $arrLawyer[$objUser->id] = $objUser->name;
                        }
                    }

                    //nur als neu markeiren wenn es für den Mandant neu ist und nicht für den Anwalt
                    $boolNew = ('1' === $objRecords->new && 'tl_user' === $objRecords->authorTable) ? true : false;

                    //uploads
                    $arrRecordFiles = $RecordHelper->getLinksFromUpload($objRecords->upload);

                    //Akten-Eigenchaften zusammenstellen
                    $arrItems[] = [
                        'recordId' => $objRecords->id,
                        'userId' => $objRecords->userId,
                        'caseId' => $objRecords->pid,
                        'senderName' => $senderName,
                        'casetitle' => $objCase->title,
                        'caseNumber' => $objCase->caseNumber,
                        'dateSend' => $objRecords->dateSend,
                        'new' => $boolNew,
                        'boolFiles' => ($arrRecordFiles && \count($arrRecordFiles) > 0) ? true : false,
                        'files' => $arrRecordFiles,
                        'subject' => $objRecords->subject,
                        'text' => $objRecords->text,
                        'deleteAllow' => $deleteAllowed,
                        'toCaseUrl' => $casePagePath.'#case_'.$objRecords->pid,
                    ];
                }
            } else {
                $this->Template->message = $GLOBALS['TL_LANG']['tl_lcp_case_record']['no_entries_msg'];
                $this->Template->alertType = 'danger';
            }
        }

        // Daten für "neue Nachricht" sammeln
        $arrForm = [
            'arrCases' => $this->getCaseOptionsForNewRecord($Member->id),
            'arrLawyer' => $RecordHelper->getLawyerSelectOptions(),
        ];

        //Falls ein Entwurf gespeichert wurde
        $objDraft = LcpCaseRecordModel::findDraftRecordsByMember($Member->id);

        if (null !== $objDraft) {
            $arrForm['recordId'] = $objDraft->id;
            $arrForm['activeLawyer'] = $objDraft->userId;
            $arrForm['activeCase'] = $objDraft->pid;
            $arrForm['subject'] = $objDraft->subject;
            $arrForm['message'] = html_entity_decode($objDraft->text);

            $arrUploads = [];
            if (null !== $objDraft->upload) {
                $arrUploads = unserialize($objDraft->upload);
            }

            $arrDzFiles = [];
            if (\is_array($arrUploads) && \count($arrUploads) > 0) {
                $arrForm['dokumente'] = \count($arrUploads);
                $arrForm['attachfiles'] = html_entity_decode(json_encode($arrUploads));

                $uploadFilePath = FilesModel::findByUuid($objSettings->uploadFolder)->path.'/'.$Member->id;

                foreach ($arrUploads as $ufile) {
                    $obj['name'] = $ufile;
                    $obj['size'] = filesize(TL_ROOT.'/'.$uploadFilePath.'/'.$ufile);
                    $obj['path'] = $uploadFilePath;
                    $arrDzFiles[] = $obj;
                }
            }
        }

        $this->Template->headline = $this->headline;
        $this->Template->newRecordFormId = $newRecordFormId;
        $this->Template->listRecordFormId = $listRecordFormId;
        $this->Template->isDraft = (null === $objDraft) ? false : true;
        $this->Template->form = $arrForm;
        $this->Template->items = $arrItems;
        $this->Template->total = $this->total;
        $this->Template->search = $dbSearch;
        $this->Template->uploadFileExtensions = $objSettings->uploadFileExtensions;
        $this->Template->uploadFileSize = $objSettings->uploadFileSize;
        $this->Template->uploadMaxFiles = $objSettings->uploadMaxFiles;
        $this->Template->beforeUploadText = $objSettings->beforeUploadText;
        $this->Template->arrDzFiles = $arrDzFiles;

        //Aktions-Benachrichtigungen fürs Template setzen
        if ($_COOKIE['LCP_MESSAGE']) {
            $this->Template->message = $_COOKIE['LCP_MESSAGE'];
            setcookie('LCP_MESSAGE', '', time() - 1000000);
            unset($_COOKIE['LCP_MESSAGE']);
        }

        if ($_COOKIE['LCP_ALERT_TYPE']) {
            $this->Template->alertType = $_COOKIE['LCP_ALERT_TYPE'];
            setcookie('LCP_ALERT_TYPE', '', time() - 1000000);
            unset($_COOKIE['LCP_ALERT_TYPE']);
        }

        // add TinyMCE-Files
        $GLOBALS['TL_JAVASCRIPT']['tinymce'] = 'assets/tinymce4/js/tinymce.min.js';
        $GLOBALS['TL_BODY'][] = "
        <script>
            tinymce.init({
              selector: 'textarea#message_text',
              skin: 'contao',
              height: 130,
              menubar: false,
              plugins: [],
              toolbar: 'bold italic underline | removeformat'
            });
        </script>";

        // add Dropzone-Files
        $GLOBALS['TL_CSS']['dropzone_css'] = $GLOBALS['LAWYERCLIENTPORTAL_PUBLIC_FOLDER'].'/css/dropzone.css';
        $GLOBALS['TL_JAVASCRIPT']['dropzone_js'] = $GLOBALS['LAWYERCLIENTPORTAL_PUBLIC_FOLDER'].'/js/dropzone.js';

        // add custom LCP-Logic
        $GLOBALS['TL_JAVASCRIPT']['lcp'] = $GLOBALS['LAWYERCLIENTPORTAL_PUBLIC_FOLDER'].'/js/lcp.js';
    }

    protected function getCaseOptionsForNewRecord($memberId)
    {
        $arrCases = [];

        $objCases = LcpCaseModel::findPublishedByMember($memberId);
        if (null === $objCases) {
            return $arrCases;
        }
        while ($objCases->next()) {
            // fuer neue Nachricht: Fälle-Selectfeld sammeln
            $arrCases[$objCases->id] = (\strlen($objCases->title) > 0)
                ? $objCases->title.' ('.$objCases->caseNumber.')'
                : $objCases->caseNumber;
        }

        return $arrCases;
    }
}
