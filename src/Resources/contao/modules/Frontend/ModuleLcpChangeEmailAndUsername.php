<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\Modules\Frontend;

use Contao\BackendTemplate;
use Contao\Environment;
use Contao\FormPassword;
use Contao\FormTextField;
use Contao\FrontendUser;
use Contao\Input;
use Contao\MemberModel;
use Contao\Module;
use Contao\PageModel;
use Contao\StringUtil;
use Contao\System;
use Contao\Widget;

class ModuleLcpChangeEmailAndUsername extends Module
{
    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'lcp_set_username';

    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE === 'BE') {
            $objTemplate = new BackendTemplate('be_wildcard');
            $objTemplate->wildcard = '### LCP Benutzername und E-Mail setzen ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id='.$this->id;

            return $objTemplate->parse();
        }

        // Return if there is no logged in user
        if (!FE_USER_LOGGED_IN) {
            return '';
        }

        return parent::generate();
    }

    /**
     * Generate the module.
     *
     * @throws \Exception
     */
    protected function compile(): void
    {
        /* @var PageModel $objPage */
        global $objPage;

        $this->import(FrontendUser::class, 'User');

        $GLOBALS['TL_LANGUAGE'] = $objPage->language;

        System::loadLanguageFile('tl_member');
        $this->loadDataContainer('tl_member');

        unset($GLOBALS['TL_DCA']['tl_member']['fields']['email']['label']);
        // New email widget
        $arrFields['newEmail'] = $GLOBALS['TL_DCA']['tl_member']['fields']['email'];
        $arrFields['newEmail']['name'] = 'email';
        unset($arrFields['newEmail']['label']);
        $arrFields['newEmail']['label'] = 'Neue E-Mail-Adresse';

        // New email confirm
        $arrFields['newEmailConfirm'] = $GLOBALS['TL_DCA']['tl_member']['fields']['email'];
        $arrFields['newEmailConfirm']['name'] = 'email_confirm';
        unset($arrFields['newEmailConfirm']['label']);
        $arrFields['newEmailConfirm']['label'] = 'E-Mail bestätigen';
//        dump($GLOBALS['TL_DCA']['tl_member']['fields']['email']);

        // Old password widget
        $arrFields['password'] = [
            'name' => 'password',
            'label' => &$GLOBALS['TL_LANG']['MSC']['password'],
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'preserveTags' => true, 'hideInput' => true],
        ];

        $row = 0;
        $doNotSubmit = false;
        $objMember = MemberModel::findByPk($this->User->id);
        $strFormId = 'tl_change_email_'.$this->id;
        $strTable = $objMember->getTable();
        $session = System::getContainer()->get('session');
        $flashBag = $session->getFlashBag();

        /** @var FormTextField $objNewEmail */
        $objNewEmail = null;

        /** @var FormTextField $objPassword */
        $objPassword = null;

        // Initialize the widgets
        foreach ($arrFields as $strKey => $arrField) {
            /** @var Widget $strClass */
            $strClass = $GLOBALS['TL_FFL'][$arrField['inputType']];

            // Continue if the class is not defined
            if (!class_exists($strClass)) {
                continue;
            }

            $arrField['eval']['required'] = $arrField['eval']['mandatory'];

            /** @var Widget $objWidget */
            $objWidget = new $strClass($strClass::getAttributesFromDca($arrField, $arrField['name']));

            $objWidget->storeValues = true;
            $objWidget->rowClass =
                'row_'.$row.((0 === $row) ? ' row_first' : '').((0 === ($row % 2)) ? ' even' : ' odd');

            // Increase the row count if it is a password field
            if ($objWidget instanceof FormPassword) {
                $objWidget->rowClassConfirm = 'row_'.++$row.((0 === ($row % 2)) ? ' even' : ' odd');
            }

            ++$row;

            // Store the widget objects
            $strVar = 'obj'.ucfirst($strKey);
            $$strVar = $objWidget;

            // Validate the widget
            if (Input::post('FORM_SUBMIT') === $strFormId) {
                $objWidget->validate();

                // Validate the password
                if ('password' === $strKey) {
                    $encoder = System::getContainer()->get('security.encoder_factory')->getEncoder(FrontendUser::class);

                    if (!$encoder->isPasswordValid($objMember->password, $objWidget->value, null)) {
                        $objWidget->value = '';
                        $objWidget->addError('Das Passwort war nicht richtig.');
                        sleep(2); // Wait 2 seconds while brute forcing :)
                    }
                }

                if ($objWidget->hasErrors()) {
                    $doNotSubmit = true;
                }
            }
            $arrWidgets[$strKey] = $objWidget->parse();
//            dump($objWidget);
        }

        $this->Template->fieldWidgets = $arrWidgets;
        $this->Template->confirm_error = false;

        //pruefen ob beide eintraege gleich sind
        if (Input::post('FORM_SUBMIT') === $strFormId && !$doNotSubmit) {
            if (Input::post('email') !== Input::post('email_confirm')) {
                $doNotSubmit = true;
                $this->Template->confirm_error = true;
                $this->Template->confirm_message = 'Die E-Mail-Wiederholung stimmte nicht überein.';
            }
        }
        $this->Template->hasError = $doNotSubmit;

        // Store the new password
        if (Input::post('FORM_SUBMIT') === $strFormId && !$doNotSubmit) {
            $objMember->tstamp = time();
            $objMember->email = $objNewEmail->value;
            $objMember->username = $objNewEmail->value;
            $objMember->save();

            // HOOK: set new password callback
            if (isset($GLOBALS['TL_HOOKS']['setNewEmail']) && \is_array($GLOBALS['TL_HOOKS']['setNewEmail'])) {
                foreach ($GLOBALS['TL_HOOKS']['setNewEmail'] as $callback) {
                    $this->import($callback[0]);
                    $this->{$callback[0]}->{$callback[1]}($objMember, $objNewEmail->value, $this);
                }
            }

            // Update the current user so they are not logged out automatically
            $this->User->findBy('id', $objMember->id);

            // Check whether there is a jumpTo page
            if (($objJumpTo = $this->objModel->getRelated('jumpTo')) instanceof PageModel) {
                $this->jumpToOrReload($objJumpTo->row());
            }

            $flashBag->set(
                'mod_changeEmail_confirm',
                'Eine neue E-Mail-Adresse wurde erfolgreich gesetzt. 
                Diese muss auch zukünftig für den Login verwendet werden.'
            );
            $this->reload();
        }

        // Confirmation message
        if ($session->isStarted() && $flashBag->has('mod_changeEmail_confirm')) {
            $arrMessages = $flashBag->get('mod_changeEmail_confirm');
            $this->Template->message = $arrMessages[0];
        }

        $this->Template->formId = $strFormId;
        $this->Template->hasError = $doNotSubmit;
        $this->Template->action = Environment::get('indexFreeRequest');
        $this->Template->slabel = StringUtil::specialchars($GLOBALS['TL_LANG']['MSC']['changePassword']);
        $this->Template->rowLast = 'row_'.$row.' row_last'.((0 === ($row % 2)) ? ' even' : ' odd');
    }
}

class_alias(ModuleLcpChangeEmailAndUsername::class, 'ModuleLcpChangeEmailAndUsername');
