<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\Modules\Frontend;

use Contao\BackendTemplate;
use Contao\CoreBundle\OptIn\OptIn;
use Contao\Environment;
use Contao\FrontendTemplate;
use Contao\Input;
use Contao\MemberModel;
use Contao\Module;
use Contao\PageModel;
use Contao\StringUtil;
use Contao\System;
use Contao\Versions;
use Contao\Widget;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Front end module "lost password".
 */
class ModuleLcpPassword extends Module
{
    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'lcp_set_password';

    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE === 'BE') {
            $objTemplate = new BackendTemplate('be_wildcard');
            $objTemplate->wildcard = '### LCP Passwort für neuen Account setzen ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id='.$this->id;

            return $objTemplate->parse();
        }

        return parent::generate();
    }

    /**
     * Generate the module.
     *
     * @throws \Exception
     */
    protected function compile(): void
    {
        /* @var PageModel $objPage */
        global $objPage;

        $GLOBALS['TL_LANGUAGE'] = $objPage->language;

        System::loadLanguageFile('tl_member');
        $this->loadDataContainer('tl_member');

        // Set new password
        if (null === Input::get('token') || 0 !== strncmp(Input::get('token'), 'pw-', 3)) {
            $this->strTemplate = 'mod_message';
            $this->Template = new FrontendTemplate($this->strTemplate);
            $this->Template->type = 'error';
            $this->Template->message = $GLOBALS['TL_LANG']['MSC']['invalidToken'];

            return;
        }

        $this->setNewPassword();
    }

    /**
     * Set the new password.
     *
     * @throws \Exception
     */
    protected function setNewPassword(): void
    {
        /** @var OptIn $optIn */
        $optIn = System::getContainer()->get('contao.opt-in');

        // Find an unconfirmed token with only one related record
        if ((!$optInToken = $optIn->find(Input::get('token')))
            || !$optInToken->isValid()
            || 1 !== \count($arrRelated = $optInToken->getRelatedRecords())
            || 'tl_member' !== key($arrRelated)
            || 1 !== \count($arrIds = current($arrRelated))
            || (!$objMember = MemberModel::findByPk($arrIds[0]))) {
            $this->strTemplate = 'mod_message';

            $this->Template = new FrontendTemplate($this->strTemplate);
            $this->Template->type = 'error';
            $this->Template->message = $GLOBALS['TL_LANG']['MSC']['invalidToken'];

            return;
        }

        if ($optInToken->isConfirmed()) {
            $this->strTemplate = 'mod_message';

            $this->Template = new FrontendTemplate($this->strTemplate);
            $this->Template->type = 'error';
            $this->Template->message = $GLOBALS['TL_LANG']['MSC']['tokenConfirmed'];

            return;
        }

        if ($optInToken->getEmail() !== $objMember->email) {
            $this->strTemplate = 'mod_message';

            $this->Template = new FrontendTemplate($this->strTemplate);
            $this->Template->type = 'error';
            $this->Template->message = $GLOBALS['TL_LANG']['MSC']['tokenEmailMismatch'];

            return;
        }

        // Initialize the versioning (see #8301)
        $objVersions = new Versions('tl_member', $objMember->id);
        $objVersions->setUsername($objMember->username);
        $objVersions->setUserId(0);
        $objVersions->setEditUrl('contao/main.php?do=member&act=edit&id=%s&rt=1');
        $objVersions->initialize();

        // Define the form field
        $arrField = $GLOBALS['TL_DCA']['tl_member']['fields']['password'];

        /** @var Widget $strClass */
        $strClass = $GLOBALS['TL_FFL']['password'];

        // Fallback to default if the class is not defined
        if (!class_exists($strClass)) {
            $strClass = 'FormPassword';
        }

        /** @var Widget $objWidget */
        $objWidget = new $strClass($strClass::getAttributesFromDca($arrField, 'password'));

        // Set row classes
        $objWidget->rowClass = 'row_0 row_first even';
        $objWidget->rowClassConfirm = 'row_1 odd';
        $this->Template->rowLast = 'row_2 row_last even';

        /** @var Session $objSession */
        $objSession = System::getContainer()->get('session');

        // Validate the field
        if (Input::post('FORM_SUBMIT')
            && Input::post('FORM_SUBMIT') === $objSession->get('setPasswordToken')) {
            $objWidget->validate();

            // Set the new password and redirect
            if (!$objWidget->hasErrors()) {
                $objSession->set('setPasswordToken', '');

                $objMember->tstamp = time();
                $objMember->locked = 0; // see #8545
                $objMember->password = $objWidget->value;
                $objMember->save();

                $optInToken->confirm();

                // Create a new version
                if ($GLOBALS['TL_DCA']['tl_member']['config']['enableVersioning']) {
                    $objVersions->create();
                }

                // HOOK: set new password callback
                if (isset($GLOBALS['TL_HOOKS']['setNewPassword'])
                    && \is_array($GLOBALS['TL_HOOKS']['setNewPassword'])) {
                    foreach ($GLOBALS['TL_HOOKS']['setNewPassword'] as $callback) {
                        $this->import($callback[0]);
                        $this->{$callback[0]}->{$callback[1]}($objMember, $objWidget->value, $this);
                    }
                }

                // Redirect to the jumpTo page
                if (($objTarget = $this->objModel->getRelated('reg_jumpTo')) instanceof PageModel) {
                    /* @var PageModel $objTarget */
                    $this->redirect($objTarget->getFrontendUrl());
                }

                // Confirm
                $this->strTemplate = 'mod_message';

                $this->Template = new FrontendTemplate($this->strTemplate);
                $this->Template->type = 'confirm';
                $this->Template->message = $GLOBALS['TL_LANG']['MSC']['newPasswordSet'];

                return;
            }
        }

        $strToken = md5(uniqid((string) mt_rand(), true));
        $objSession->set('setPasswordToken', $strToken);

        $this->Template->formId = $strToken;
        $this->Template->fields = $objWidget->parse();
        $this->Template->action = Environment::get('indexFreeRequest');
        $this->Template->slabel = StringUtil::specialchars($GLOBALS['TL_LANG']['MSC']['setNewPassword']);
    }
}

class_alias(ModuleLcpPassword::class, 'ModuleLcpPassword');
