<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\Modules\Frontend;

use Contao\BackendTemplate;
use Contao\Controller;
use Contao\FrontendUser;
use Contao\Input;
use Contao\Module;
use Contao\PageModel;
use Contao\UserModel;
use Srhinow\LawyerClientPortal\EventListener\Dca\LcpSetting;
use Srhinow\LawyerClientPortal\Helper\CaseHelper;
use Srhinow\LawyerClientPortal\Model\LcpCaseModel;
use Srhinow\LawyerClientPortal\Model\LcpCaseRecordModel;

class ModuleCaseList extends ModuleLcpFrontend
{
    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'lcp_case_list';

    /**
     * Target pages.
     *
     * @var array
     */
    protected $arrTargets = [];

    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE === 'BE') {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### MANDANT AKTEN LISTE ###';

            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=modules&amp;act=edit&amp;id='.$this->id;

            return $objTemplate->parse();
        }

        // Fallback template
        if ($this->lcp_fe_template) {
            $this->strTemplate = $this->lcp_fe_template;
        }

        // Set the item from the auto_item parameter
        if ($GLOBALS['TL_CONFIG']['useAutoItem'] && \Input::get('auto_item')) {
            \Input::setGet('pid', \Input::get('auto_item'));
        }

        return parent::generate();
    }

    /**
     * Generate module.
     *
     * @throws \Exception
     */
    protected function compile(): void
    {
        if (!FE_USER_LOGGED_IN) {
            return;
        }

        // einzelner Download
        $this->runOneDownload();

        // multi-Actions
        $this->runMultiAction();

        $User = FrontendUser::getInstance();
        $this->loadLanguageFile('tl_lcp_case_record');
        $arrItems = $arrProjects = $arrProjIds = [];

        //RecordPage-Url generieren
        $objRecordPage = PageModel::findByPk($this->jumpToRecordList);
        $recordPagePath = (null === $objRecordPage) ? $_SERVER['REQUEST_URI'] : $objRecordPage->getFrontendUrl();

        //Einstellungen zur Berechnung von Closed and Download until
        $objSettings = LcpSetting::getLcpSettings();

        // Get the total number of items
        $total = LcpCaseModel::countPublishedByMember($User->id);

        if ($total > 0) {
            $this->setListPagination();

            $options = [];

            //falls eine Sortierung übergeben wurde diese setzen
            $sorting = Input::post('sorting');
            if (isset($sorting) && \strlen($sorting) > 0) {
                $options['order'] = $sorting;
            }

            $objCases = LcpCaseModel::findPublishedByMember($User->id, $this->limit, $this->offset, $options);
            $arrUser = [];
            if (null !== $objCases) {
                $isClosedCase = false;

                while ($objCases->next()) {
                    if (!$arrUser[$objCases->userId]) {
                        $objUser = UserModel::findByPk($objCases->userId);
                        $arrUser[$objCases->userId] = (null === $objUser) ? '' : $objUser->name;
                    }

                    //für das template ob die lock-icon-Spalte eingefügt werden soll
                    if (empty($objCases->stop)) {
                        $isClosedCase = true;
                    }

                    // downloaden bis-Datum berechnen
                    $downloadUntil = empty($objCases->stop)
                        ? ''
                        : strtotime('+'.$objSettings->closedAfterDays.' days', (int) $objCases->stop);

                    //Akten-Eigenchaften zusammenstellen
                    $arrItems[] = [
                        'caseId' => $objCases->id,
                        'caseTitle' => $objCases->title,
                        'caseNumber' => $objCases->caseNumber,
                        'dateAdded' => $objCases->dateAdded,
                        'recordUrl' => $recordPagePath.'?case='.$objCases->id,
                        'countAllPosts' => LcpCaseRecordModel::countAllPostedCaseRecords($objCases),
                        'countNewPosts' => LcpCaseRecordModel::countNewPostedCaseRecords($objCases),
                        'lawyer' => $arrUser[$objCases->userId],
                        'closed' => $objCases->stop,
                        'downloadUntil' => $downloadUntil,
                        'notice' => $objCases->notice,
                    ];
                }
            }
        }

        $this->Template->headline = $this->headline;
        $this->Template->isOneClosed = $isClosedCase;
        $this->Template->items = $arrItems;
        $this->Template->total = $total;
        $this->Template->message = $GLOBALS['TL_LANG']['tl_lcp_case']['no_entries_msg'];

        $GLOBALS['TL_JAVASCRIPT']['lcp'] = $GLOBALS['LAWYERCLIENTPORTAL_PUBLIC_FOLDER'].'/js/lcp.js';
    }

    /**
     * falls übergeben wird ein zip-Download angestoßen.
     *
     * @throws \Exception
     */
    protected function runOneDownload(): void
    {
        $downloadOne = Input::post('download_one');
        if ((int) $downloadOne > 0) {
            $objCase = LcpCaseModel::findByPk($downloadOne);
            $CaseHelper = new CaseHelper();
            $zipPath = $CaseHelper->createOneCaseToZip($objCase);

            if (file_exists(TL_ROOT.\DIRECTORY_SEPARATOR.$zipPath)) {
                Controller::sendFileToBrowser($zipPath);
            }
        }
    }

    /**
     * falls gesetzt wird ein von den ausgewählten Actions ausgeführt.
     *
     * @throws \Exception
     */
    protected function runMultiAction(): void
    {
        $action = Input::post('action');
        $actionItems = Input::post('items');
        if (\strlen((string) $action) && null !== $actionItems) {
            switch ($action) {
                case 'download':
                    $CaseHelper = new CaseHelper();
                    // Zip-Dateien erstellen und deren Pfade im Array sammeln
                    $zipPath = $CaseHelper->createMultiCaseToZip($actionItems);
                    if (file_exists(TL_ROOT.\DIRECTORY_SEPARATOR.$zipPath)) {
                        Controller::sendFileToBrowser($zipPath);
                    }
                    break;
            }
        }
    }
}
