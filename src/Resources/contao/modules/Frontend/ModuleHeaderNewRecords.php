<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\Modules\Frontend;

use Contao\BackendTemplate;
use Contao\FrontendUser;
use Contao\Module;
use Contao\PageModel;
use Srhinow\LawyerClientPortal\Model\LcpCaseRecordModel;

class ModuleHeaderNewRecords extends Module
{
    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'lcp_header_new_records';

    /**
     * Target pages.
     *
     * @var array
     */
    protected $arrTargets = [];

    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE === 'BE') {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### HEADER :: NEUE NACHRICHTEN ###';

            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=modules&amp;act=edit&amp;id='.$this->id;

            return $objTemplate->parse();
        }

        // Fallback template
        if ($this->lcp_fe_template) {
            $this->strTemplate = $this->lcp_fe_template;
        }

        return parent::generate();
    }

    /**
     * Generate module.
     */
    protected function compile(): void
    {
        if (!FE_USER_LOGGED_IN) {
            return;
        }

        $User = FrontendUser::getInstance();

        //RecordPage-Url generieren
        $objRecordPage = PageModel::findByPk($this->jumpToRecordList);
        $recordPagePath = (null === $objRecordPage) ? $_SERVER['REQUEST_URI'] : $objRecordPage->getFrontendUrl();

        $this->Template->headline = $this->headline;
        $this->Template->recordPagePath = $recordPagePath;
        $this->Template->total = LcpCaseRecordModel::countAllNewPostedRecordsByMemberId($User->id);
    }
}
