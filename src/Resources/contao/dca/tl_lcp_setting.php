<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

/*
 * Table tl_lcp_setting.
 */
$GLOBALS['TL_DCA']['tl_lcp_setting'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'enableVersioning' => false,
        'sql' => [
            'keys' => [
                'id' => 'primary',
            ],
        ],
        'onload_callback' => [
            ['srhinow.lawyer_client_portal.listener.dca.lcp_settings', 'onLoadCallback'],
        ],
    ],
    // List
    'list' => [
        'sorting' => [
            'panelLayout' => 'filter;search,limit',
        ],
        'label' => [
            'fields' => ['userGroupId', 'memberGroupId'],
            'format' => '%s (%s)',
        ],
    ],
    // Palettes
    'palettes' => [
        'default' => '
            {groups_legend},userGroupId,memberGroupId
            ;{folder_legend},placeForCases,tmpFolder,uploadFolder
            ;{notification_legend},sendTokenNotification,reg_jumpTo,activateNotification,newRecordNotification
            ;{upload_legend},beforeUploadText,uploadFileExtensions,uploadFileSize,uploadMaxFiles
            ;{cron_legend},cronTime,closedAfterDays
        ',
    ],

    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'modify' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'userGroupId' => [
            'inputType' => 'select',
            'foreignKey' => 'tl_user_group.name',
            'eval' => [
                'doNotCopy' => true,
                'mandatory' => true,
                'chosen' => true,
                'includeBlankOption' => true,
                'tl_class' => 'w50',
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
        ],
        'memberGroupId' => [
            'inputType' => 'select',
            'foreignKey' => 'tl_member_group.name',
            'eval' => [
                'chosen' => true,
                'mandatory' => true,
                'includeBlankOption' => true,
                'tl_class' => 'w50',
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
        ],
        'placeForCases' => [
            'exclude' => true,
            'inputType' => 'fileTree',
            'eval' => [
                'mandatory' => true,
                'multiple' => false,
                'fieldType' => 'radio',
                'tl_class' => 'clr',
            ],
            'sql' => 'blob NULL',
        ],
        'tmpFolder' => [
            'exclude' => true,
            'inputType' => 'fileTree',
            'eval' => [
                'mandatory' => true,
                'multiple' => false,
                'fieldType' => 'radio',
                'tl_class' => 'clr',
            ],
            'sql' => 'blob NULL',
        ],
        'uploadFolder' => [
            'exclude' => true,
            'inputType' => 'fileTree',
            'eval' => [
                'mandatory' => true,
                'multiple' => false,
                'fieldType' => 'radio',
                'tl_class' => 'clr',
            ],
            'sql' => 'blob NULL',
        ],
        'activateNotification' => [
            'exclude' => true,
            'inputType' => 'select',
            'foreignKey' => 'tl_nc_notification.title',
            'eval' => [
                'includeBlankOption' => true,
                'tl_class' => 'w50',
                'submitOnChange' => false,
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
        ],
        'sendTokenNotification' => [
            'exclude' => true,
            'inputType' => 'select',
            'foreignKey' => 'tl_nc_notification.title',
            'eval' => [
                'includeBlankOption' => true,
                'tl_class' => 'w50',
                'submitOnChange' => false,
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
        ],
        'reg_jumpTo' => [
            'exclude' => true,
            'inputType' => 'pageTree',
            'foreignKey' => 'tl_page.title',
            'eval' => ['fieldType' => 'radio', 'tl_class' => 'clr'],
            'sql' => 'int(10) unsigned NOT NULL default 0',
            'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
        ],
        'newRecordNotification' => [
            'exclude' => true,
            'inputType' => 'select',
            'foreignKey' => 'tl_nc_notification.title',
            'eval' => [
                'includeBlankOption' => true,
                'tl_class' => 'w50',
                'submitOnChange' => false,
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
        ],
        'cronTime' => [
            'search' => true,
            'inputType' => 'text',
            'default' => '16:00',
            'eval' => ['maxlength' => 25, 'tl_class' => 'w50'],
            'sql' => "varchar(25) NOT NULL default '16:00'",
        ],
        'uploadFileExtensions' => [
            'inputType' => 'text',
            'default' => '.jpeg,.png,.pdf,.docx,.odt,.doc',
            'eval' => ['maxlength' => 255, 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default '.jpg,.jpeg,.png,.pdf,.docx,.odt,.doc'",
        ],
        'uploadFileSize' => [
            'inputType' => 'text',
            'default' => 5,
            'eval' => ['tl_class' => 'w50'],
            'sql' => "int(10) unsigned NOT NULL default '5'",
        ],
        'uploadMaxFiles' => [
            'inputType' => 'text',
            'default' => 5,
            'eval' => ['tl_class' => 'w50'],
            'sql' => "int(10) unsigned NOT NULL default '5'",
        ],
        'beforeUploadText' => [
            'default' => '',
            'search' => true,
            'inputType' => 'textarea',
            'eval' => ['maxlength' => 500, 'tl_class' => 'clr full', 'rte' => 'tinyMCE', 'style' => 'height:100px'],
            'sql' => 'text NULL',
        ],
        'closedAfterDays' => [
            'search' => true,
            'inputType' => 'text',
            'default' => '30',
            'eval' => ['maxlength' => 25, 'tl_class' => 'w50'],
            'sql' => "varchar(25) NOT NULL default '30'",
        ],
    ],
];
