<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

/*
 * Table tl_user_group.
 */

// Extend the default palette
Contao\CoreBundle\DataContainer\PaletteManipulator::create()
    ->addLegend(
        'lcpcaseportal_legend',
        'amg_legend',
        Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_BEFORE
    )
    ->addField(
        [
            'lcp_casep',
            'lcp_case_recordp',
        ],
        'lcpcaseportal_legend',
        Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND
    )
    ->applyToPalette('default', 'tl_user_group')
;

// Add fields to tl_user_group

$GLOBALS['TL_DCA']['tl_user_group']['fields']['lcp_casep'] = [
    'exclude' => true,
    'inputType' => 'checkbox',
    'options' => ['create', 'edit', 'delete'],
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval' => ['multiple' => true],
    'sql' => 'blob NULL',
];

$GLOBALS['TL_DCA']['tl_user_group']['fields']['lcp_case_recordp'] = [
    'exclude' => true,
    'inputType' => 'checkbox',
    'options' => ['create', 'edit', 'delete'],
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval' => ['multiple' => true],
    'sql' => 'blob NULL',
];
