<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

/*
 * Table tl_user.
 */

// Extend the default palettes
Contao\CoreBundle\DataContainer\PaletteManipulator::create()
    ->addLegend(
        'lcpcaseportal_legend',
        'tl_user',
        Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_BEFORE
    )
    ->addField(
        [
            'lcp_casep',
            'lcp_case_recordp',
        ],
        'lcpcaseportal_legend',
        Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND
    )
    ->applyToPalette('extend', 'tl_user')
    ->applyToPalette('custom', 'tl_user')
;

$GLOBALS['TL_DCA']['tl_user']['palettes']['admin'] .= ';{teaser_legend:hide},contentTeaser';
$GLOBALS['TL_DCA']['tl_user']['palettes']['group'] .= ';{teaser_legend:hide},contentTeaser';

foreach ($GLOBALS['TL_DCA']['tl_user']['palettes'] as $type => $string) {
    $GLOBALS['TL_DCA']['tl_user']['palettes'][$type] = str_replace(
        ',name,',
        ',name,shortName,',
        $GLOBALS['TL_DCA']['tl_user']['palettes'][$type]
    );
}

// Add fields to tl_user
$GLOBALS['TL_DCA']['tl_user']['fields']['lcp_casep'] = [
    'exclude' => true,
    'inputType' => 'checkbox',
    'options' => ['edit', 'create', 'delete'],
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval' => ['multiple' => true],
    'sql' => 'blob NULL',
];

$GLOBALS['TL_DCA']['tl_user']['fields']['lcp_case_recordp'] = [
    'exclude' => true,
    'inputType' => 'checkbox',
    'options' => ['edit', 'create', 'delete'],
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval' => ['multiple' => true],
    'sql' => 'blob NULL',
];

$GLOBALS['TL_DCA']['tl_user']['fields']['contentTeaser'] = [
    'exclude' => true,
    'inputType' => 'select',
    'options_callback' => ['srhinow.lawyer_client_portal.listener.dca.user', 'getAlias'],
    'eval' => ['chosen' => true, 'tl_class' => 'w50', 'includeBlankOption' => true],
    'sql' => 'int(10) unsigned NOT NULL default 0',
];
$GLOBALS['TL_DCA']['tl_user']['fields']['shortName'] = [
    'exclude' => true,
    'inputType' => 'text',
    'eval' => ['tl_class' => 'w50'],
    'sql' => "varchar(125) NOT NULL default ''",
];
