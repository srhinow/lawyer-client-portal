<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

/*
 * Table tl_lcp_case.
 */
$GLOBALS['TL_DCA']['tl_lcp_case'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'ctable' => 'tl_lcp_case_record',
        'enableVersioning' => true,
        'sql' => [
            'keys' => [
                'id' => 'primary',
                'memberId' => 'index',
            ],
        ],
    ],
    // List
    'list' => [
        'sorting' => [
            'mode' => 2,
            'fields' => ['dateAdded DESC'],
            'flag' => 1,
            'panelLayout' => 'filter;sort,search,limit',
        ],
        'label' => [
            'fields' => ['caseNumber', 'title', 'dateAdded', 'tstamp', 'start', 'memberId', 'published'],
            'showColumns' => true,
        ],
        'global_operations' => [
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();"',
            ],
        ],
        'operations' => [
            'edit' => [
                'label' => &$GLOBALS['TL_LANG']['tl_lcp_case']['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.gif',
            ],
            'records' => [
                'label' => &$GLOBALS['TL_LANG']['tl_lcp_case']['records'],
                'href' => 'table=tl_lcp_case_record',
                'icon' => $GLOBALS['LAWYERCLIENTPORTAL_PUBLIC_FOLDER'].'/icons/emails_stack.png',
            ],
            'compress' => [
                'label' => &$GLOBALS['TL_LANG']['tl_lcp_case']['compress'],
                'href' => 'key=compress',
                'icon' => $GLOBALS['LAWYERCLIENTPORTAL_PUBLIC_FOLDER'].'/icons/compress.png',
            ],
            'delete' => [
                'label' => &$GLOBALS['TL_LANG']['tl_lcp_case']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => '
                onclick="if (!confirm(\''.$GLOBALS['TL_LANG']['MSC']['deleteConfirm'].'\')) return false; 
                Backend.getScrollOffset();"',
            ],
        ],
    ],
    // Palettes
    'palettes' => [
        'default' => '
            {meta_legend},caseNumber,title
            ;{member_user_legend},memberId,userId        
            ;{notice_legend},notice
            ;{published_legend},start,stop,published
            ;{expert_legend:hide},caseFolder
            ',
    ],

    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'title' => [
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => ['tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'tstamp' => [
            'label' => &$GLOBALS['TL_LANG']['tl_lcp_case']['tstamp'],
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'flag' => 8,
            'eval' => ['mandatory' => true, 'rgxp' => 'date'],
        ],
        'modify' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'flag' => 8,
            'eval' => [
                'mandatory' => true,
                'datepicker' => $this->getDatePickerString(),
                'tl_class' => 'wizard w50',
                'minlength' => 1,
                'maxlength' => 10,
                'rgxp' => 'date',
            ],
        ],
        'dateAdded' => [
            'default' => time(),
            'sorting' => true,
            'flag' => 6,
            'eval' => ['rgxp' => 'date', 'doNotCopy' => true],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'caseNumber' => [
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => ['tl_class' => 'w50'],
            'sql' => "varchar(35) NOT NULL default ''",
        ],
        'createdFrom' => [
            'default' => BackendUser::getInstance()->id,
            'search' => true,
            'filter' => true,
            'sorting' => true,
            'foreignKey' => 'tl_user.name',
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
        ],
        'memberId' => [
            'inputType' => 'select',
            'search' => true,
            'filter' => true,
            'sorting' => true,
            'foreignKey' => 'tl_member.CONCAT(lastname, \' \', firstname, \' \', email\' \', company)',
            'options_callback' => ['srhinow.lawyer_client_portal.listener.dca.member', 'getClientMemberAsOptions'],
            'eval' => ['chosen' => true, 'mandatory' => true, 'includeBlankOption' => true, 'tl_class' => 'w50'],
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
        ],
        'userId' => [
            'inputType' => 'select',
            'default' => BackendUser::getInstance()->id,
            'search' => true,
            'filter' => true,
            'sorting' => true,
            'foreignKey' => 'tl_user.name',
            'options_callback' => ['srhinow.lawyer_client_portal.listener.dca.user', 'getLawyerUserAsOptions'],
            'eval' => ['chosen' => true, 'mandatory' => true, 'includeBlankOption' => true, 'tl_class' => 'w50'],
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
        ],
        'notice' => [
            'default' => '',
            'search' => true,
            'inputType' => 'textarea',
            'eval' => ['maxlength' => 500, 'tl_class' => 'clr full', 'rte' => 'tinyMCE', 'style' => 'height:100px'],
            'sql' => 'text NULL',
        ],
        'published' => [
            'exclude' => true,
            'filter' => true,
            'sorting' => true,
            'default' => 1,
            'inputType' => 'checkbox',
            'eval' => ['doNotCopy' => true, 'tl_class' => 'clr w50'],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'start' => [
            'exclude' => true,
            'sorting' => true,
            'default' => time(),
            'inputType' => 'text',
            'flag' => 8,
            'eval' => ['rgxp' => 'datim', 'datepicker' => true, 'tl_class' => 'w50 wizard'],
            'sql' => "varchar(10) NOT NULL default ''",
        ],
        'stop' => [
            'exclude' => true,
            'sorting' => true,
            'inputType' => 'text',
            'flag' => 8,
            'eval' => ['rgxp' => 'datim', 'datepicker' => true, 'tl_class' => 'w50 wizard'],
            'sql' => "varchar(10) NOT NULL default ''",
        ],
        'caseFolder' => [
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => ['tl_class' => 'w50', 'readonly' => true],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
    ],
];
