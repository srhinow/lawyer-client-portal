<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

use Srhinow\LawyerClientPortal\EventListener\Dca\LcpSetting;

/*
 * Table tl_lcp_case_record
 */
$GLOBALS['TL_DCA']['tl_lcp_case_record'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'ptable' => 'tl_lcp_case',
        'enableVersioning' => true,
        'sql' => [
            'keys' => [
                'id' => 'primary',
                'pid,authorId' => 'index',
            ],
        ],
    ],
    // List
    'list' => [
        'sorting' => [
            'mode' => 4,
            'headerFields' => ['title', 'userId:tl_user.name', 'published', 'start', 'stop'],
            'fields' => ['tstamp DESC'],
            'panelLayout' => 'filter;sort,search,limit',
        ],
        'global_operations' => [
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();"',
            ],
        ],
        'operations' => [
            'edit' => [
                'label' => &$GLOBALS['TL_LANG']['tl_lcp_case_record']['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.gif',
            ],
            'show' => [
                'label' => &$GLOBALS['TL_LANG']['tl_lcp_case_record']['show'],
                'href' => 'key=readMessage',
                'icon' => $GLOBALS['LAWYERCLIENTPORTAL_PUBLIC_FOLDER'].'/icons/table_tab_search.png',
            ],
            'delete' => [
                'label' => &$GLOBALS['TL_LANG']['tl_lcp_case_record']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => 'onclick="
                if (!confirm(\''.$GLOBALS['TL_LANG']['MSC']['deleteConfirm'].'\')) return false; 
                Backend.getScrollOffset();"',
            ],
        ],
    ],
    // Palettes
    'palettes' => [
        '__selector__' => ['readyToSend'],
        'default' => '
        {main_legend},userId,memberId,pid
        ;{message_legend},subject,text
        ;{upload_legend},upload
        ;{member_option_legend:hide},deleted
        ;{published_legend},sendBySave,readyToSend
        ',
    ],
    // Subpalettes
    'subpalettes' => [
        'readyToSend' => 'sendByDate',
    ],
    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'pid' => [
            'search' => true,
            'inputType' => 'select',
            'foreignKey' => 'tl_lcp_case.CONCAT(title, \' \',  caseNumber)',
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'belongsTo', 'load' => 'lazy'],
            'eval' => [
                'mandatory' => true,
                'chosen' => true,
                'submitOnChange' => true,
                'includeBlankOption' => true,
                'tl_class' => 'w50',
            ],
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'flag' => 8,
            'eval' => ['mandatory' => true, 'rgxp' => 'datim'],
        ],
        'modify' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'flag' => 8,
            'eval' => [
                'mandatory' => true,
                'datepicker' => $this->getDatePickerString(),
                'tl_class' => 'wizard w50', 'minlength' => 1,
                'maxlength' => 10, 'rgxp' => 'date',
            ],
        ],
        'dateAdded' => [
            'default' => time(),
            'flag' => 6,
            'sorting' => true,
            'eval' => ['rgxp' => 'date', 'doNotCopy' => true],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'dateSend' => [
            'rangeFilter' => true,
            'flag' => 6,
            'eval' => ['rgxp' => 'datim', 'doNotCopy' => true, 'emptyIsNull' => true],
            'sql' => 'int(10) unsigned NULL',
        ],
        'dateOpen' => [
            'flag' => 6,
            'eval' => ['rgxp' => 'datim', 'doNotCopy' => true, 'emptyIsNull' => true],
            'sql' => 'int(10) unsigned NULL',
        ],
        'userId' => [
            'default' => BackendUser::getInstance()->id,
            'foreignKey' => 'tl_user.name',
            'inputType' => 'select',
            'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
            'eval' => [
                'doNotCopy' => true,
                'mandatory' => true,
                'chosen' => true,
                'includeBlankOption' => true,
                'tl_class' => 'w50',
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'memberId' => [
            'foreignKey' => 'tl_member.CONCAT(lastname, \' \', firstname, \' \', email,\' \',id)',
            'inputType' => 'select',
            'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
            'eval' => [
                'doNotCopy' => true,
                'mandatory' => true,
                'chosen' => true,
                'includeBlankOption' => true,
                'submitOnChange' => true,
                'tl_class' => 'w50',
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'authorId' => [
            'default' => BackendUser::getInstance()->id,
            'inputType' => 'select',
            'foreignKey' => 'tl_user.name',
            'eval' => [
                'doNotCopy' => true,
                'mandatory' => false,
                'chosen' => true,
                'includeBlankOption' => true,
                'tl_class' => 'w50',
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
        ],
        'authorName' => [
            'search' => true,
            'inputType' => 'text',
            'eval' => ['maxlength' => 255, 'tl_class' => 'w50', 'readonly' => true],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'authorTable' => [
            'inputType' => 'select',
            'options' => ['tl_user', 'tl_member'],
            'eval' => ['maxlength' => 255, 'tl_class' => 'w50'],
            'sql' => "varchar(55) NOT NULL default ''",
        ],
        'subject' => [
            'search' => true,
            'inputType' => 'text',
            'eval' => ['maxlength' => 255, 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'text' => [
            'search' => true,
            'default' => '',
            'inputType' => 'textarea',
            'eval' => [
                'tl_class' => 'clr full',
                'rte' => 'tinyMCE',
                'style' => 'height:100px',
            ],
            'sql' => 'text NULL',
        ],
        'upload' => [
            'inputType' => 'fineUploader',
            'eval' => [
                // Mandatory to store the file on the server
                'storeFile' => true,
                // Allow multiple files to be uploaded
                'multiple' => true,
                // Upload target directory (can also be a Contao file system UUID)
                'uploadFolder' => LcpSetting::getLcpUploadPath(),
                // Upload to the FE member home directory (overrides "uploadFolder",
                // can also be a Contao file system UUID)
                'useHomeDir' => false,
                // Custom uploader configuration that gets merged with the other params
                'uploaderConfig' => "['debug': true]",
                // Maximum files that can be uploaded
                'uploaderLimit' => 10,
                // Add files to the database assisted file system
                'addToDbafs' => true,
                'extensions' => &$GLOBALS['TL_CONFIG']['uploadTypes'],           // Allowed extension types
                // 'minlength'         => 1048000, // Minimum file size
                //'maxlength'         => 2048000, // Maximum file size (is ignored if you use chunking!)
                //'maxWidth'          => 800, // Maximum width (applies to images only)
                //'maxHeight'         => 600, // Maximum height (applies to images only)
                // Do not overwrite files in destination folder
                'doNotOverwrite' => true,
                // Custom upload button label
                'uploadButtonLabel' => &$GLOBALS['TL_LANG']['tl_lcp_case_record']['upload_button_text'],
                // Enable chunking
                'chunking' => true,
                // Chunk size in bytes
                'chunkSize' => 2000000,
                // Allow multiple chunks to be uploaded simultaneously per file
                'concurrent' => true,
                // Maximum allowable concurrent requests
                'maxConnections' => 5,
                // Upload the files directly to the destination folder. If not set, then the files are first uploaded
                // to the temporary folder and moved to the destination folder only when the form is submitted
                // 'directUpload' => true,
                // Set a custom thumbnail image size that is generated upon image upload
                // 'imageSize'         => [160, 120, 'center_center'],

                // You can also use the default features of fileTree widget such as:
                // isGallery, isDownloads

                // The "orderField" attribute is not valid (see #9)
            ],
            'sql' => 'blob NULL',
        ],
        'new' => [
            'inputType' => 'checkbox',
            'filter' => true,
            'eval' => ['doNotCopy' => true, 'tl_class' => 'clr'],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'draft' => [
            'inputType' => 'checkbox',
            'eval' => ['doNotCopy' => true, 'tl_class' => 'clr'],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'deleted' => [
            'inputType' => 'checkbox',
            'filter' => true,
            'eval' => ['doNotCopy' => true, 'tl_class' => 'clr'],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'sendBySave' => [
            'inputType' => 'checkbox',
            'filter' => true,
            'eval' => ['doNotCopy' => true, 'tl_class' => 'clr'],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'readyToSend' => [
            'inputType' => 'checkbox',
            'filter' => true,
            'eval' => ['doNotCopy' => true, 'tl_class' => 'clr', 'submitOnChange' => true],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'sendByDate' => [
            'default' => time(),
            'exclude' => true,
            'filter' => true,
            'sorting' => true,
            'flag' => 8,
            'inputType' => 'text',
            'eval' => [
                'rgxp' => 'datim',
                'mandatory' => true,
                'doNotCopy' => true,
                'datepicker' => true,
                'tl_class' => 'w50 wizard',
            ],
            'sql' => 'int(10) unsigned NOT NULL default 0',
        ],
        'sendToEmail' => [
            'search' => true,
            'inputType' => 'text',
            'eval' => ['maxlength' => 255],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'sendToId' => [
            'inputType' => 'select',
            'foreignKey' => 'tl_member.firstname tl_member.lastname',
            'eval' => [
                'doNotCopy' => true,
                'mandatory' => true,
                'chosen' => true,
                'includeBlankOption' => true,
                'tl_class' => 'w50',
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
        ],
    ],
];
