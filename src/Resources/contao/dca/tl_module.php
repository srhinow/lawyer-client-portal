<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

$GLOBALS['TL_DCA']['tl_module']['palettes']['lcp_fe_case_list'] = '
    {title_legend},name,headline,type,lcp_numberOfItems,perPage;
    {jumpTo_Legend},jumpToRecordList;
    {template_legend},lcp_fe_template;
    {protected_legend:hide},protected;
    {expert_legend:hide},cssID,space';
$GLOBALS['TL_DCA']['tl_module']['palettes']['lcp_fe_header_new_record'] = '
    {title_legend},name,headline,type;
    {jumpTo_Legend},jumpToRecordList;
    {template_legend},lcp_fe_template;
    {protected_legend:hide},protected;
    {expert_legend:hide},cssID,space';
$GLOBALS['TL_DCA']['tl_module']['palettes']['lcp_fe_lawyer_teaser'] = '
    {title_legend},name,headline,type;
    {template_legend},lcp_fe_template;
    {protected_legend:hide},protected;
    {expert_legend:hide},cssID,space';
$GLOBALS['TL_DCA']['tl_module']['palettes']['lcp_fe_record_list'] = '
    {title_legend},type,name,headline,lcp_numberOfItems,perPage;
    {jumpTo_Legend},jumpToCaseList;
    {template_legend},lcp_fe_template;
    {protected_legend:hide},protected;
    {expert_legend:hide},cssID,space';
$GLOBALS['TL_DCA']['tl_module']['palettes']['lcp_fe_set_password'] = '
    {title_legend},name,headline,type;
    {email_legend:hide},reg_jumpTo;
    {template_legend:hide},customTpl;
    {protected_legend:hide},protected;
    {expert_legend:hide},guests,cssID';
$GLOBALS['TL_DCA']['tl_module']['palettes']['lcp_fe_set_username'] = '
    {title_legend},name,headline,type;
    {email_legend:hide},reg_jumpTo;
    {template_legend:hide},customTpl;
    {protected_legend:hide},protected;
    {expert_legend:hide},guests,cssID';

$GLOBALS['TL_DCA']['tl_module']['fields']['lcp_fe_template'] = [
    'exclude' => true,
    'inputType' => 'select',
    'options_callback' => ['srhinow.lawyer_client_portal.listener.dca.module', 'getLcpTemplates'],
    'eval' => ['tl_class' => 'w50'],
    'sql' => "varchar(32) NOT NULL default ''",
];

$GLOBALS['TL_DCA']['tl_module']['fields']['lcp_numberOfItems'] = [
    'default' => 3,
    'exclude' => true,
    'inputType' => 'text',
    'eval' => ['mandatory' => true, 'rgxp' => 'digit', 'tl_class' => 'w50'],
    'sql' => "smallint(5) unsigned NOT NULL default '0'",
];
$GLOBALS['TL_DCA']['tl_module']['fields']['jumpToCaseList'] = [
    'exclude' => true,
    'inputType' => 'pageTree',
    'foreignKey' => 'tl_page.title',
    'eval' => ['fieldType' => 'radio'],
    'sql' => 'int(10) unsigned NOT NULL default 0',
    'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
];
$GLOBALS['TL_DCA']['tl_module']['fields']['jumpToRecordList'] = [
    'exclude' => true,
    'inputType' => 'pageTree',
    'foreignKey' => 'tl_page.title',
    'eval' => ['fieldType' => 'radio'],
    'sql' => 'int(10) unsigned NOT NULL default 0',
    'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
];
