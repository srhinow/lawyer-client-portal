<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\Automator;

use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\Folder;
use Contao\System;
use Psr\Log\LogLevel;
use Srhinow\LawyerClientPortal\EventListener\Dca\LcpSetting;

class LcpAutomator extends System
{
    /**
     * Make the constuctor public.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @throws \Exception
     */
    public function cleanCaseTempFolder(): void
    {
        $tmpPath = LcpSetting::getLcpTempPath();
        $objFolder = new Folder($tmpPath);
        $objFolder->purge();

        // Add a log entry
        $logger = static::getContainer()->get('monolog.logger.contao');
        $logger->log(
            LogLevel::INFO,
            'Purged the LCP Export-Folder',
            ['contao' => new ContaoContext(__METHOD__, TL_CRON)]
        );
    }

    public function cleanUploadFolder(): void
    {
        $uploadPath = LcpSetting::getLcpUploadPath();
        $logger = static::getContainer()->get('monolog.logger.contao');

        try {
            $objFolder = new Folder($uploadPath);
            $objFolder->purge();

            // Add a log entry
            $logger->log(
                LogLevel::INFO,
                'Purged the LCP Upload-Folder',
                ['contao' => new ContaoContext(__METHOD__, TL_GENERAL)]
            );
        } catch (\Exception $e) {
            // Add a log entry
            $logger->log(
                LogLevel::ERROR,
                'NO Purged the LCP Upload-Folder:'.$uploadPath,
                ['contao' => new ContaoContext(__METHOD__, TL_ERROR)]
            );
        }
    }
}
