<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\EventListener\Hook;

use Contao\BackendTemplate;
use Contao\Controller;
use Srhinow\LawyerClientPortal\Model\LcpCaseRecordModel;
use Terminal42\ServiceAnnotationBundle\ServiceAnnotationInterface;

class GetSystemMessagesListener implements ServiceAnnotationInterface
{
    /**
     * @Hook("getSystemMessages")
     */
    public function onGetSystemMessages(): string
    {
        $objTemplate = new BackendTemplate('be_dashboard_new_posts');

        $objNewPosts = LcpCaseRecordModel::findAllNewMemberRecords();
        if (null === $objNewPosts) {
            $objTemplate->isNewPosts = false;
        } else {
            $objTemplate->isNewPosts = true;

            while ($objNewPosts->next()) {
                $newPosts[] = [
                    'date' => date('d.m.y H:i', (int) $objNewPosts->dateAdded),
                    'author' => $objNewPosts->authorName,
                    'subject' => $objNewPosts->subject,
                    'caseLink' => Controller::addToUrl('do=lcp_case&table=tl_lcp_case_record&id='.$objNewPosts->pid),
                ];
            }
            $objTemplate->newPosts = $newPosts;
        }

        return $objTemplate->parse();
    }
}
