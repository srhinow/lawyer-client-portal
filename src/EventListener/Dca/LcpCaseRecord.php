<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\EventListener\Dca;

use Contao\BackendTemplate;
use Contao\BackendUser;
use Contao\Config;
use Contao\Controller;
use Contao\CoreBundle\Exception\AccessDeniedException;
use Contao\DataContainer;
use Contao\Folder;
use Contao\Image;
use Contao\Input;
use Contao\MemberModel;
use Contao\Message;
use Contao\StringUtil;
use Contao\System;
use Contao\UserModel;
use Psr\Log\LogLevel;
use Srhinow\LawyerClientPortal\Helper\Helper;
use Srhinow\LawyerClientPortal\Model\LcpCaseModel;
use Srhinow\LawyerClientPortal\Model\LcpCaseRecordModel;

class LcpCaseRecord
{
    protected $User;

    /**
     * all dca config > onload_callback entries.
     */
    public function onLoadCallback(DataContainer $dc): void
    {
        $this->checkPermission();
        $this->openFile();
        $this->setChangesForAllRecordLists($dc);
        $this->setChangesForInbox($dc);
        $this->setChangesForDraft($dc);
        $this->setChangesForOutbox($dc);
        $this->setRightPathToUpload($dc);
    }

    /**
     * all dca config > onsubmit_callback entries.
     *
     * @throws \Exception
     */
    public function onSubmitCallback(DataContainer $dc): void
    {
        $this->setAuthorFields($dc);
        $this->cleanRecordFiles($dc);
        $this->sendNewRecordNotification($dc);
    }

    /**
     * den Mandant im zugeoerenden Case holen.
     *
     * @return array
     */
    public function onMemberIdLoadCallback($value, DataContainer $dc)
    {
        if ($value || 'lcp_case' !== Input::get('do')) {
            return $value;
        }

        $objCase = LcpCaseModel::findByPk($dc->activeRecord->pid);

        return (null === $objCase) ? $value : $objCase->memberId;
    }

    /**
     * Options for authorId-Field.
     *
     * @return array
     */
    public function onAuthorIdOptionsCallback(DataContainer $dc)
    {
        $user = new User();

        return $user->getLawyerUserAsOptions();
    }

    public function onCaseLoadCallback($value, DataContainer $dc)
    {
        if ($value || 'lcp_case' !== Input::get('do')) {
            return $value;
        }

        $objCase = LcpCaseModel::findByPk($dc->activeRecord->pid);

        return (null === $objCase) ? $value : $objCase->id;
    }

    /**
     * Options for pid.
     *
     * @return array
     */
    public function onCaseOptionsCallback(DataContainer $dc)
    {
        if ('lcp_case' === Input::get('do')) {
            return $this->getCaseOptionsForCaseRecords($dc);
        }

        return $this->getCaseOptionsForInbox($dc);
    }

    /**
     * format child-entries in list-view.
     *
     * @param array $arrRow
     *
     * @return string
     */
    public function onSortingChildRecordCallback($arrRow)
    {
        $strContent = '<strong>'.$arrRow['authorName'].':</strong> '.StringUtil::substr($arrRow['subject'], 100);
        $ifUploads = false;

        //falls es auch uploads gibt
        if ($arrLinks = $this->getLinksFromUpload($arrRow['upload'])) {
            $ifUploads = true;
        }

        if (true === $ifUploads) {
            $strContent .= '<br><strong>Anlagen:</strong> '.implode(', ', $arrLinks);
        }

        //CSS-Klassen setzen
        $strClasses = 'case_record';
        if ('1' === $arrRow['deleted']) {
            $strClasses .= ' deleted';
        }
        if ('1' === $arrRow['new']) {
            $strClasses .= ' new';
        }

        return sprintf('<div class="'.$strClasses.'">%s</div>', $strContent);
    }

    /**
     * if click on file-link from a record.
     */
    public function openFile(): void
    {
        $path = Input::get('file');
        if ('openfile' !== Input::get('key') || !$path) {
            return;
        }

        Controller::sendFileToBrowser($path);
    }

    /**
     * @param string $uploadField
     *
     * @return array|bool
     */
    public function getLinksFromUpload($uploadField)
    {
        $arrLinks = [];

        if (null === $uploadField) {
            return $arrLinks;
        }

        $arrUploads = unserialize($uploadField);

        if (!\is_array($arrUploads)) {
            return $arrLinks;
        }

        foreach ($arrUploads as $k => $upload) {
            if (!\strlen($upload)) {
                continue;
            }
            $objFile = \FilesModel::findByUuid($upload);

            $url = Controller::addToUrl('key=openfile&file='.$objFile->path);
            $arrLinks[] = '<a href="'.$url.'">'.$objFile->name.'</a>';
        }

        return (\count($arrLinks) < 0) ? false : $arrLinks;
    }

    public function setRightPathToUpload(DataContainer $dc): void
    {
        //aktuelles Record-Object holen
        $objRecord = LcpCaseRecordModel::findByPk($dc->id);
        if (null === $objRecord) {
            return;
        }

        //wenn z.B. es in der Inbox eine neue Nachricht mit POST als Case erstellt werden soll
        $CaseId = ($objRecord->pid) ?: Input::post('pid');

        //für den Akte-File-Path die Eltern-Tabelle holen
        $objCase = LcpCaseModel::findByPk($objRecord->pid);
        if (null === $objCase || \strlen($objCase->caseFolder) < 1) {
            return;
        }

        $GLOBALS['TL_DCA']['tl_lcp_case_record']['fields']['upload']['eval']['uploadFolder'] = $objCase->caseFolder;
    }

    /**
     * Check permissions to edit table tl_spree_case.
     */
    public function checkPermission(): void
    {
        $this->User = BackendUser::getInstance();

        if ($this->User->isAdmin) {
            return;
        }

        // Set root IDs
        if (empty($this->User->lcp_case_records) || !\is_array($this->User->lcp_case_records)) {
            $root = [0];
        } else {
            $root = $this->User->lcp_case_records;
        }

        // den Button "neuer Fall" ausblenden wenn die notwendigen Berechtigungen fehlen
        if (!$this->User->hasAccess('create', 'lcp_case_recordp')) {
            $GLOBALS['TL_DCA']['tl_spree_case']['config']['closed'] = true;
        }

        $objSession = System::getContainer()->get('session');
        $errorMsg = 'Not enough permissions to %s Laywer-Client-Portal case ID %s.';
        // Check current action
        switch (Input::get('act')) {
            case 'select':
            case 'show':
            case '':
                // Allow
                break;
            case 'edit':
                // Check permissions to add lcp_case_records
                if (!$this->User->hasAccess('edit', 'lcp_case_recordp')) {
                    throw new AccessDeniedException(sprintf($errorMsg, Input::get('act'), Input::get('id')));
                }
                break;
            case 'create':
                // Check permissions to add lcp_case_records
                if (!$this->User->hasAccess('create', 'lcp_case_recordp')) {
                    throw new AccessDeniedException(sprintf($errorMsg, Input::get('act'), Input::get('id')));
                }
                break;
            case 'copy':
                // Check permissions to add lcp_case_records
                if (!$this->User->hasAccess('copy', 'lcp_case_recordp')) {
                    throw new AccessDeniedException(sprintf($errorMsg, Input::get('act'), Input::get('id')));
                }
                // no break
            case 'delete':
                // Check permissions to add lcp_case_records
                if (!$this->User->hasAccess('delete', 'lcp_case_recordp')) {
                    throw new AccessDeniedException(sprintf($errorMsg, Input::get('act'), Input::get('id')));
                }
                break;

            case 'editAll':
            case 'deleteAll':
            case 'overrideAll':
                $session = $objSession->all();
                if ('deleteAll' === Input::get('act')
                    && !$this->User->hasAccess('delete', 'lcp_case_recordp')) {
                    $session['CURRENT']['IDS'] = [];
                } else {
                    $session['CURRENT']['IDS'] = array_intersect((array) $session['CURRENT']['IDS'], $root);
                }
                $objSession->replace($session);
                break;

            default:
                $errorMsg = 'Not enough permissions to %s lcp_case_records.';
                if (\strlen(Input::get('act')) > 0) {
                    throw new AccessDeniedException(sprintf($errorMsg, Input::get('act')));
                }
                break;
        }
    }

    /**
     * Return the edit case button.
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function onEditButtonCallback($row, $href, $label, $title, $icon, $attributes)
    {
        $this->User = BackendUser::getInstance();

        return (
            $this->User->isAdmin ||
            (
            $this->User->hasAccess('edit', 'lcp_case_recordp')
            && null === $row['dateSend']
            && $row['authorTable'] = 'tl_user'
            && $row['authorId'] === $this->User->id)) ? sprintf(
                '<a href="%s" title="%s" %s>%s</a>',
                Controller::addToUrl($href.'&amp;id='.$row['id'].'&amp;rt='.REQUEST_TOKEN),
                StringUtil::specialchars($title),
                $attributes,
                Image::getHtml($icon, $label)
            ) : '';
    }

    /**
     * Return the delete case button.
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function onDeleteButtonCallback($row, $href, $label, $title, $icon, $attributes)
    {
        $this->User = BackendUser::getInstance();

        return (
            $this->User->isAdmin || ($this->User->hasAccess('delete', 'lcp_case_recordp'))) ? sprintf(
                '<a href="%s" title="%s" %s>%s</a>',
                Controller::addToUrl($href.'&amp;id='.$row['id'].'&amp;rt='.REQUEST_TOKEN),
                StringUtil::specialchars($title),
                $attributes,
                Image::getHtml($icon, $label)
            ) : '';
    }

    /**
     * Set the timestamp to 00:00:00 (see #26).
     *
     * @param int $value
     *
     * @return int
     */
    public function loadDate($value)
    {
        $objSettings = LcpSetting::getLcpSettings();
        $value = (0 === $value) ? time() : $value;

        return strtotime(date('Y-m-d', (int) $value).$objSettings->cronTime.':00');
    }

    /**
     * setzt beim speichern die Felder authorName und authorTable.
     */
    public function setAuthorFields(DataContainer $dc): void
    {
        $authorId = $dc->activeRecord->authorId;
        if (0 === (int) $authorId) {
            return;
        }

        $objCaseRecord = LcpCaseRecordModel::findByPk($dc->id);
        if (null === $objCaseRecord) {
            return;
        }

        $objCase = LcpCaseModel::findByPk($dc->activeRecord->pid);
        if (null === $objCase) {
            return;
        }

        $objUser = UserModel::findByPk($authorId);
        if (null === $objUser) {
            return;
        }

        $objCaseRecord->memberId = $objCase->memberId;
        $objCaseRecord->authorId = $objUser->id;
        $objCaseRecord->authorName = $objUser->name;
        $objCaseRecord->authorTable = 'tl_user';
        $objCaseRecord->save();
    }

    public function setChangesForAllRecordLists(DataContainer $dc): void
    {
        //keine Entwürfe der Mandanten anzeigen
        $GLOBALS['TL_DCA']['tl_lcp_case_record']['list']['sorting']['filter'][] = ['draft =?', ''];
    }

    /**
     * veraendert das DCA wenn case-Table - unabhaengig alle eingegangenen Nachrichten angezeigt werden sollen.
     */
    public function setChangesForInbox(DataContainer $dc): void
    {
        //nur dca verändern wenn im allRecord-View
        if ('lcp_inbox' !== Input::get('do')) {
            return;
        }

        //config -Anpassungen
        unset($GLOBALS['TL_DCA']['tl_lcp_case_record']['config']['ptable']);

        //List - Anpassungen
        $GLOBALS['TL_DCA']['tl_lcp_case_record']['list']['sorting']['mode'] = 1;
        $GLOBALS['TL_DCA']['tl_lcp_case_record']['list']['sorting']['filter'][] = ['authorTable=?', 'tl_member'];
        $GLOBALS['TL_DCA']['tl_lcp_case_record']['list']['sorting']['filter'][] = ['dateSend IS NOT NULL AND 1=?', 1];
        $GLOBALS['TL_DCA']['tl_lcp_case_record']['list']['label'] =
        [
            'fields' => ['pid:tl_lcp_case.CONCAT(title, \' (\', caseNumber, \') \')', 'tstamp', 'dateSend', 'subject'],
            'showColumns' => true,
            'label_callback' => ['srhinow.lawyer_client_portal.listener.dca.lcp_case_record', 'labelLabelCallback'],
        ];

        unset($GLOBALS['TL_DCA']['tl_lcp_case_record']['list']['operations']['edit']);

        array_insert(
            $GLOBALS['TL_DCA']['tl_lcp_case_record']['list']['operations'],
            1,
            $this->getCaseRecordDcaOperation()
        );

        //Fields-Anpassungen
        $GLOBALS['TL_DCA']['tl_lcp_case_record']['fields']['pid']['label']
            = &$GLOBALS['TL_LANG']['tl_lcp_case_record']['case'];

        $this->setRightPathToUpload($dc);
//        dump($objCase->caseFolder);
//        die();
    }

    /**
     * veraendert das DCA wenn case-Table - alle noch nicht gesendeten aber gespeicherten Nachrichten listen.
     */
    public function setChangesForDraft(DataContainer $dc): void
    {
        //nur dca verändern wenn im allRecord-View
        if ('lcp_draft' !== Input::get('do')) {
            return;
        }

        //config -Anpassungen
        unset($GLOBALS['TL_DCA']['tl_lcp_case_record']['config']['ptable']);

        //List - Anpassungen
        $GLOBALS['TL_DCA']['tl_lcp_case_record']['list']['sorting']['mode'] = 1;
        $GLOBALS['TL_DCA']['tl_lcp_case_record']['list']['sorting']['filter'][] = ['authorTable=?', 'tl_user'];
        $GLOBALS['TL_DCA']['tl_lcp_case_record']['list']['sorting']['filter'][] = ['dateSend IS NULL AND 1=?', 1];
        $GLOBALS['TL_DCA']['tl_lcp_case_record']['list']['sorting']['filter'][] = ['readyToSend =?', ''];
        $GLOBALS['TL_DCA']['tl_lcp_case_record']['list']['label'] =
        [
            'fields' => [
                'pid:tl_lcp_case.CONCAT(title, \' (\', caseNumber, \') \')',
                'memberId:tl_member.CONCAT(lastname, \' \', firstname, \' \', email)',
                'tstamp',
                'subject',
            ],
            'showColumns' => true,
        ];
        array_insert(
            $GLOBALS['TL_DCA']['tl_lcp_case_record']['list']['operations'],
            1,
            $this->getCaseRecordDcaOperation()
        );

        //Fields-Anpassungen
        $GLOBALS['TL_DCA']['tl_lcp_case_record']['fields']['pid']['label']
            = &$GLOBALS['TL_LANG']['tl_lcp_case_record']['case'];

        $this->setRightPathToUpload($dc);
    }

    /**
     * veraendert das DCA wenn case-Table - unabhaengig fuer alle ausgeheneden Nachrichten angezeigt werden sollen.
     */
    public function setChangesForOutbox(DataContainer $dc): void
    {
        //nur dca verändern wenn im allRecord-View
        if ('lcp_outbox' !== Input::get('do')) {
            return;
        }

        //config -Anpassungen
        unset($GLOBALS['TL_DCA']['tl_lcp_case_record']['config']['ptable']);

        //List - Anpassungen
        $GLOBALS['TL_DCA']['tl_lcp_case_record']['list']['sorting']['mode'] = 1;
        $GLOBALS['TL_DCA']['tl_lcp_case_record']['list']['sorting']['filter'][] =
            ['authorTable = ?', 'tl_user'];
        $GLOBALS['TL_DCA']['tl_lcp_case_record']['list']['sorting']['filter'][] =
            ['dateSend IS NOT NULL OR readyToSend = ?', '1'];

        $GLOBALS['TL_DCA']['tl_lcp_case_record']['list']['label'] =
        [
            'fields' => ['pid:tl_lcp_case.CONCAT(title, \' (\', caseNumber, \') \')', 'tstamp', 'dateSend', 'subject'],
            'showColumns' => true,
            'label_callback' => ['srhinow.lawyer_client_portal.listener.dca.lcp_case_record', 'labelLabelCallback'],
        ];

        unset($GLOBALS['TL_DCA']['tl_lcp_case_record']['list']['operations']['edit']);

        array_insert(
            $GLOBALS['TL_DCA']['tl_lcp_case_record']['list']['operations'],
            1,
            $this->getCaseRecordDcaOperation()
        );

        //Fields-Anpassungen
        $GLOBALS['TL_DCA']['tl_lcp_case_record']['fields']['pid']['label']
            = &$GLOBALS['TL_LANG']['tl_lcp_case_record']['case'];

        $this->setRightPathToUpload($dc);
    }

    /**
     * @param $arrRow
     * @param $strLabel
     * @param $dc
     * @param $args
     */
    public function labelLabelCallback($arrRow, $strLabel, $dc, $args)
    {
        if ($arrRow['new']) {
            foreach ($args as $k => $arg) {
                $args[$k] = '<span class="strong">'.$arg.'</span>';
            }
        }

        return $args;
    }

    /**
     * Um die jeweilige passende Fall-ID an die URL zuhaengen,
     * da der Button-Callback aus der Methode $this->setChangesForInbox() gesetzt wird.
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function onListOperationsCaseRecordsButtonCallback($row, $href, $label, $title, $icon, $attributes)
    {
        if (\in_array(Input::get('do'), ['lcp_inbox', 'lcp_outbox'], true)) {
            $objRecord = LcpCaseRecordModel::findByPk($row['id']);

            if (null !== $objRecord) {
                return
                    sprintf(
                        '<a href="%s" title="%s" %s>%s</a>',
                        Controller::addToUrl($href.'&amp;id='.$objRecord->pid),
                        StringUtil::specialchars($title),
                        $attributes,
                        Image::getHtml($icon, $label)
                    );
            }
        }

        return '';
    }

    /**
     * Damit für die Nachricht als gelesen markiert/ gespeichert.
     */
    public function setRecordAsOpen(\Contao\Model $objRecord = null): void
    {
        if (null === $objRecord) {
            return;
        }

        $objRecord->dateOpen = time();
        $objRecord->new = '';
        $objRecord->save();
    }

    /**
     * Die Modal-Anzeige des info-Buttons für die Anzeige der jeweiligen Nachricht aufbereiten.
     *
     * @return string|void
     */
    public function readMessage(DataContainer $dc)
    {
        if ('readMessage' !== Input::get('key')) {
            return;
        }

        $objRecord = LcpCaseRecordModel::findByPk($dc->id);
        if (null === $objRecord) {
            return;
        }

        $this->setRecordAsOpen($objRecord);

        $objCase = LcpCaseModel::findByPk($objRecord->pid);
        $objLawyer = UserModel::findByPk($objRecord->userId);

        $Template = new BackendTemplate('be_readMessage_popup');
        $Template->subject = $objRecord->subject;
        $Template->author = $objRecord->authorName;
        $Template->lawyer = $objLawyer->name;
        $Template->caseNumber = $objCase->caseNumber;
        $Template->createDate = date('d.m.Y H:i', (int) $objRecord->dateAdded);
        $Template->dateSend = ($objRecord->dateSend) ? date('d.m.Y H:i', (int) $objRecord->dateSend) : '--';
        $Template->dateOpen = ($objRecord->dateOpen) ? date('d.m.Y H:i', (int) $objRecord->dateOpen) : '--';
        $Template->message = StringUtil::restoreBasicEntities($objRecord->text);

        if ($arrLinks = $this->getLinksFromUpload($objRecord->upload)) {
            $Template->isAttachment = true;
            $Template->attachments = implode("<br>\n", $arrLinks);
        } else {
            $Template->isAttachment = false;
        }

        return $Template->parse();
    }

    /**
     * alle Dateien die nicht mehr zur Nachricht gespeichert sind ->löschen.
     *
     * @throws \Exception
     */
    public function cleanRecordFiles(DataContainer $dc): void
    {
        $fileNames = [];

        $objCurrentRecord = LcpCaseRecordModel::findByPk($dc->id);
        if (null === $objCurrentRecord) {
            return;
        }

        // Akte Datensatz zur Nachricht holen
        $objCase = LcpCaseModel::findByPk($objCurrentRecord->pid);
        if (null === $objCase) {
            return;
        }

        //alle Nachrichten zu dem Fall holen
        $objRecords = LcpCaseRecordModel::findBy(['pid=?'], [$objCase->id]);
        if (null === $objRecords) {
            return;
        }

        while ($objRecords->next()) {
            if (null === $objRecords->upload) {
                continue;
            }

            $uploads = unserialize($objRecords->upload);

            //Wenn Dateien zur Nachricht gespeichert wurden in $fileNames aufnehmen
            if (\is_array($uploads) && \count($uploads) > 0) {
                foreach ($uploads as $uuid) {
                    $objFile = \FilesModel::findByUuid($uuid);
                    if (null === $objFile) {
                        continue;
                    }

                    $fileNames[] = $objFile->name;
                }
            }
        }

        // Folder als Objekt holen
        $CaseFolder = new Folder($objCase->caseFolder);
        if (null === $CaseFolder) {
            return;
        }

        //Dateien aus dem Ordner auslesen
        $Files = scandir(TL_ROOT.'/'.$CaseFolder->path);
        if (!\is_array($Files) || \count($Files) < 1) {
            return;
        }

        //nicht zu loeschende Dateien in die ignore-Liste aufnehmen
        $ignoreDrop = array_merge($fileNames, ['.', '..']);

        // alle Dateien die nicht ingoriert werden sollen -> loeschen
        $isDelete = false;
        foreach ($Files as $file) {
            if (\in_array($file, $ignoreDrop, true)) {
                continue;
            }
            $filePath = TL_ROOT.\DIRECTORY_SEPARATOR.$objCase->caseFolder.\DIRECTORY_SEPARATOR.$file;

            if (unlink($filePath)) {
                $isDelete = true;
            }
        }

        //Nachrichten setzen wenn dateien geöscht wurden
        if ($isDelete) {
            Controller::loadLanguageFile('tl_lcp_case_record');
            $msg = $GLOBALS['TL_LANG']['tl_lcp_case_record']['delete_files_success'];
            // Einmal als Log-Eintrag
            System::getContainer()
                ->get('monolog.logger.contao')
                ->log(LogLevel::INFO, $msg)
            ;
            //Einmal als Hinweis für den backendUser
            Message::addInfo($msg);
        }
    }

    /**
     * ausführen wenn die Nachricht sofort gesendet werden soll.
     */
    public function sendNewRecordNotification(DataContainer $dc): void
    {
        //wenn nicht gesetzt dann hier abbrechen

        if ('1' !== $dc->activeRecord->sendBySave) {
            return;
        }

        //den dazu gehoerenden Case-Record-Datensatz holen
        $objRecords = LcpCaseRecordModel::findByPk($dc->id);
        if (null === $objRecords) {
            return;
        }

        //den (Mandanten) Member-Datensatz holen
        $objMember = MemberModel::findByPk($dc->activeRecord->memberId);
        if (null === $objMember) {
            return;
        }

        $objSettings = LcpSetting::getLcpSettings();
        $arrToken = ['send_to' => $objMember->email];
        Helper::sendNotification($objSettings->newRecordNotification, $arrToken);

        $objRecords->new = 1;
        $objRecords->sendBySave = '';
        $objRecords->dateSend = time();
        $objRecords->sendToId = $objMember->id;
        $objRecords->sendToEmail = $objMember->email;
        $objRecords->save();

        Message::addInfo('Die "neue Nachricht" - E-Mail wurde gesendet.');
    }

    protected function getCaseOptionsForCaseRecords(DataContainer $dc)
    {
        $returnOptions = [];

        $objCase = LcpCaseModel::findBy(['published=?'], [1]);
        if (null === $objCase) {
            return $returnOptions;
        }

        while ($objCase->next()) {
            $returnOptions[$objCase->id] = (\strlen($objCase->title) > 0)
                ? $objCase->title.' ('.$objCase->caseNumber.')'
                : $objCase->caseNumber;
        }

        return $returnOptions;
    }

    protected function getCaseOptionsForInbox(DataContainer $dc)
    {
        $returnOptions = [];
        if ($dc->activeRecord->memberId < 1) {
            return ['' => 'Es muss ein Mandant gesetzt werden.'];
        }

        $objCase = LcpCaseModel::findBy(['memberId=?'], [$dc->activeRecord->memberId]);
        if (null === $objCase) {
            return ['' => 'Es muss erst eine Akte  zu dem Mandant angelegt werden.'];
        }

        while ($objCase->next()) {
            $returnOptions[$objCase->id] = (\strlen($objCase->title) > 0)
                ? $objCase->title.' ('.$objCase->caseNumber.')'
                : $objCase->caseNumber;
        }

        return $returnOptions;
    }

    protected function getCaseRecordDcaOperation()
    {
        $cr['case_records'] = [
            'label' => &$GLOBALS['TL_LANG']['tl_lcp_case_record']['case_records'],
            'href' => 'do=lcp_case&table=tl_lcp_case_record',
            'icon' => $GLOBALS['LAWYERCLIENTPORTAL_PUBLIC_FOLDER'].'/icons/account_level_filtering.png',
            'button_callback' => [
                'srhinow.lawyer_client_portal.listener.dca.lcp_case_record',
                'onListOperationsCaseRecordsButtonCallback',
            ],
        ];

        return $cr;
    }
}
