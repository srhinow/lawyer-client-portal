<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\EventListener\Dca;

use Contao\Database;
use Contao\DataContainer;
use Contao\MemberModel;
use Srhinow\LawyerClientPortal\Helper\MemberHelper;
use Srhinow\LawyerClientPortal\Model\LcpSettingModel;

class Member
{
    /**
     * all dca config > onsubmit_callback entries.
     *
     * @throws \Exception
     */
    public function onSubmitCallback($dc): void
    {
        if ($dc instanceof DataContainer) {
            $this->sendTokenEmail($dc);
        }
    }

    /**
     * wird vom DCA aufgerufen um alle Mandanten zu holen die der Gruppe aus den Settings angehoeren.
     *
     * @return array
     */
    public function getClientMemberAsOptions()
    {
        $options = [];

        //Settings holen
        $objSettings = LcpSettingModel::findByPk($GLOBALS['LAWYERCLIENTPORTAL_SETTINGS']['ID']);
        if (null === $objSettings || (int) $objSettings->memberGroupId < 1) {
            return $options;
        }

        // Mandanten holen die der Gruppe aus den Settings angehoeren
        $objMembers = Database::getInstance()
            ->execute('SELECT * FROM tl_member WHERE `groups` LIKE \'%"'.$objSettings->memberGroupId.'"%\'')
        ;
        if ($objMembers->numRows < 1) {
            return $options;
        }

        //Member in Options sammeln
        while ($objMembers->next()) {
            if (\strlen($objMembers->company) > 0) {
                $options[$objMembers->id] =
                    $objMembers->company.' ('
                    .$objMembers->firstname.' '
                    .$objMembers->lastname.') _ '
                    .$objMembers->email;
            } else {
                $options[$objMembers->id] = $objMembers->firstname.' '.$objMembers->lastname.' _ '.$objMembers->email;
            }
        }

        return $options;
    }

    public function getAllActiveMemberAsOptions()
    {
        $objMembers = MemberModel::findAll();
        $options = [];

        if (null === $objMembers) {
            return $options;
        }

        foreach ($objMembers as $member) {
            $options[$member->id] = $member->firstname.' '.$member->lastname.' _ '.$member->email;
        }

        return $options;
    }

    /**
     * beim neu anlegen eines Mandanten-Datensatzes die member-Gruppe aus den Einstellungen als default setzen.
     */
    public function getLcpSettingsMemeberGroup(DataContainer $dc)
    {
        $objSettings = LcpSettingModel::findByPk($GLOBALS['LAWYERCLIENTPORTAL_SETTINGS']['ID']);
        if (null === $objSettings) {
            return false;
        }

        return [$objSettings->memberGroupId];
    }

    public function setLcpMemeberGroup(DataContainer $dc): void
    {
        // Return if there is no active record (override all)
        if (!$dc->activeRecord || 0 === $dc->id) {
            return;
        }

        $objSettings = LcpSettingModel::findByPk($GLOBALS['LAWYERCLIENTPORTAL_SETTINGS']['ID']);
        if (null === $objSettings) {
            return;
        }

        $objMember = MemberModel::findByPk($dc->id);
        $oldGroups = (null === $objMember->groups) ? [] : unserialize($objMember->groups);
        $newGroups = array_merge($oldGroups, [$objSettings->memberGroupId]);
        $objMember->groups = $newGroups;
        $objMember->save();
    }

    /**
     * wenn checkbox gesetzt wird eine Notification mit Token-Link dem Mandanten gesendet.
     *
     * @throws \Exception
     */
    public function sendTokenEmail(DataContainer $dc): void
    {
        if ('1' !== $dc->activeRecord->sendLogin) {
            return;
        }

        $objMember = MemberModel::findByPk($dc->id);
        if (null === $objMember) {
            return;
        }

        $MemberHelper = new MemberHelper();
        $MemberHelper->sendTokenLink($objMember);

        $objMember->login = 1;
        $objMember->sendLogin = '';
        $objMember->save();
    }
}
