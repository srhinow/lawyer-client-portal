<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\EventListener\Dca;

use Contao\Controller;
use Contao\DataContainer;

class Module
{
    /**
     * Return all info templates as array.
     *
     * @return array
     */
    public function getLcpTemplates(DataContainer $dc)
    {
        return Controller::getTemplateGroup('lcp_');
    }
}
