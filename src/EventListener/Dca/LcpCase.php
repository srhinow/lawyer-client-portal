<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\EventListener\Dca;

use Contao\BackendUser;
use Contao\Controller;
use Contao\CoreBundle\Exception\AccessDeniedException;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\Database;
use Contao\DataContainer;
use Contao\FilesModel;
use Contao\Folder;
use Contao\Image;
use Contao\Input;
use Contao\MemberModel;
use Contao\Message;
use Contao\StringUtil;
use Contao\System;
use Contao\UserModel;
use Psr\Log\LogLevel;
use Srhinow\LawyerClientPortal\Helper\CaseHelper;
use Srhinow\LawyerClientPortal\Model\LcpCaseModel;
use Srhinow\LawyerClientPortal\Model\LcpSettingModel;

class LcpCase
{
    protected $User;

    /**
     * all dca config > onload_callback entries.
     */
    public function onLoadCallback(DataContainer $dc): void
    {
        $this->checkPermission();
        $this->multipleCaseExport($dc);
        $this->viewCookieMsg();
    }

    /**
     * all dca config > onsubmit_callback entries.
     */
    public function onSubmitCallback(DataContainer $dc): void
    {
        $this->setCaseFolder($dc);
    }

    /**
     * all dca config > onsubmit_callback entries.
     */
    public function onDeleteCallback(DataContainer $dc): void
    {
        $this->deleteCaseRecords($dc);
        $this->dropCaseFolder($dc);
    }

    public function onSelectButtons($arrButtons, DataContainer $dc)
    {
        return $this->addZipButton($arrButtons, $dc);
    }

    /**
     * @param $arrRow
     * @param $strLabel
     * @param $dc
     * @param $args
     */
    public function labelLabelCallback($arrRow, $strLabel, $dc, $args)
    {
        //erstellt am
        $args[2] = \strlen($arrRow['dateAdded']) ? date('d.m.y', (int) $arrRow['dateAdded']) : '--';

        //Akte-Ersteller
        $objCreator = UserModel::findByPk($arrRow['createdFrom']);
        if (null !== $objCreator) {
            $args[2] .= ' <span class="hint-text" title="'.$objCreator->email.'">(von '.$objCreator->name.')</span>';
        }

        //bearbeitet am
        $args[3] = date('d.m.y H:i', (int) $arrRow['tstamp']);

        //Akte gestartet am
        $args[4] = date('d.m.y', (int) $arrRow['start']);

        //MandantIn
        $objMandant = MemberModel::findByPk($arrRow['memberId']);
        $mandantHtml = '<span title="%s">%s %s %s</span>';
        if (null !== $objMandant) {
            $args[5] = sprintf(
                $mandantHtml,
                $objMandant->email,
                $objMandant->anrede,
                $objMandant->firstname,
                $objMandant->lastname
            );
        }

        return $args;
    }

    /**
     * Check permissions to edit table tl_spree_case.
     *
     * @throws \Contao\CoreBundle\Exception\AccessDeniedException
     */
    public function checkPermission(): void
    {
        $this->User = BackendUser::getInstance();

        if ($this->User->isAdmin) {
            return;
        }

        // Set root IDs
        if (empty($this->User->lcp_cases) || !\is_array($this->User->lcp_cases)) {
            $root = [0];
        } else {
            $root = $this->User->lcp_cases;
        }

        // den Button "neuer Fall" ausblenden wenn die notwendigen Berechtigungen fehlen
        if (!$this->User->hasAccess('create', 'lcp_casep')) {
            $GLOBALS['TL_DCA']['tl_spree_case']['config']['closed'] = true;
        }

        /** @var \Symfony\Component\HttpFoundation\Session\SessionInterface $objSession */
        $objSession = System::getContainer()->get('session');
        $errorMsg = 'Not enough permissions to %s Laywer-Client-Portal case ID %s.';
        // Check current action
        switch (Input::get('act')) {
            case 'select':
            case 'show':
            case '':
                // Allow
                break;
            case 'edit':
                // Check permissions to add tl_lcp_cases
                if (!$this->User->hasAccess('edit', 'lcp_casep')) {
                    throw new AccessDeniedException(sprintf($errorMsg, Input::get('act'), Input::get('id')));
                }
                break;
            case 'create':
                // Check permissions to add tl_lcp_cases
                if (!$this->User->hasAccess('create', 'lcp_casep')) {
                    throw new AccessDeniedException(sprintf($errorMsg, Input::get('act'), Input::get('id')));
                }
                break;
            case 'copy':
                // Check permissions to add tl_lcp_cases
                if (!$this->User->hasAccess('copy', 'lcp_casep')) {
                    throw new AccessDeniedException(sprintf($errorMsg, Input::get('act'), Input::get('id')));
                }
                // no break
            case 'delete':
                // Check permissions to add tl_lcp_cases
                if (!$this->User->hasAccess('delete', 'lcp_casep')) {
                    throw new AccessDeniedException(sprintf($errorMsg, Input::get('act'), Input::get('id')));
                }
                break;

            case 'editAll':
            case 'deleteAll':
            case 'overrideAll':
                $session = $objSession->all();
                if ('deleteAll' === Input::get('act') && !$this->User->hasAccess('delete', 'lcp_casep')) {
                    $session['CURRENT']['IDS'] = [];
                } else {
                    $session['CURRENT']['IDS'] = array_intersect((array) $session['CURRENT']['IDS'], $root);
                }
                $objSession->replace($session);
                break;

            default:
                $errorMsg = 'Not enough permissions to %s lcp_case_records.';
                if (\strlen(Input::get('act')) > 0) {
                    throw new AccessDeniedException(sprintf($errorMsg, Input::get('act')));
                }
                break;
        }
    }

    public function viewCookieMsg(): void
    {
        if ($_COOKIE['LCP_MSG']) {
            Message::addInfo($_COOKIE['LCP_MSG']);
            System::setCookie('LCP_MSG', '', time() - 60);
        }
    }

    /**
     * Return the edit case button.
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function onEditButtonCallback($row, $href, $label, $title, $icon, $attributes)
    {
        $this->User = BackendUser::getInstance();

        return $this->User->hasAccess('edit', 'lcp_casep') ?
            sprintf(
                '<a href="%s" title="%s" %s>%s</a>',
                Controller::addToUrl($href.'&amp;id='.$row['id'].'&amp;rt='.REQUEST_TOKEN),
                StringUtil::specialchars($title),
                $attributes,
                Image::getHtml($icon, $label)
            ) : '';
    }

    /**
     * Return the delete case button.
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function onDeleteButtonCallback($row, $href, $label, $title, $icon, $attributes)
    {
        $this->User = BackendUser::getInstance();

        return $this->User->hasAccess('delete', 'lcp_casep') ?
            sprintf(
                '<a href="%s" title="%s" %s>%s</a>',
                Controller::addToUrl($href.'&amp;id='.$row['id'].'&amp;rt='.REQUEST_TOKEN),
                StringUtil::specialchars($title),
                $attributes,
                Image::getHtml($icon, $label)
            ) : '';
    }

    /**
     * Den Akte-Ordner mit Settings-Pfad erzeugen und den Pfad dann in den case-Einstellungen speichern.
     *
     * @return bool|void
     */
    public function setCaseFolder(\DataContainer $dc)
    {
        //wenn der Pfad bereits angelegt wurde hier abbrechen
        if (\strlen($dc->activeRecord->caseFolder) > 0) {
            return;
        }

        //settings holen
        $objSettings = LcpSettingModel::findByPk($GLOBALS['LAWYERCLIENTPORTAL_SETTINGS']['ID']);
        if (null === $objSettings) {
            return;
        }

        //den Akten-Root-Pfad aus den Settings holen
        $objFolder = FilesModel::findByUuid($objSettings->placeForCases);
        if (null === $objFolder || !$objFolder->path) {
            return;
        }

        //Akte-Pfad zusammensetzen
        $caseNumber = Input::post('caseNumber');
        $aktePath = $objFolder->path.'/'.StringUtil::standardize($caseNumber);

        // falls nicht vorhanden den Speicherort-Ordner anlegen
        if (!file_exists(TL_ROOT.'/'.$aktePath)) {
            if (!mkdir(TL_ROOT.'/'.$aktePath)) {
                return 'Folder '.TL_ROOT.'/'.$aktePath.' existiert nicht';
            }
        }

        //aktuelle Case-Instanz holen
        $objLcpCase = LcpCaseModel::findByPk($dc->id);
        if (null === $objLcpCase) {
            return;
        }

        //Pfad zum Case speichern
        $objLcpCase->caseFolder = $aktePath;
        $objLcpCase->save();
    }

    public function deleteCaseRecords(DataContainer $dc): void
    {
        $objCase = LcpCaseModel::findByPk($dc->id);
        if (null === $objCase) {
            return;
        }

        Database::getInstance()
            ->prepare('DELETE FROM `tl_lcp_case_record` WHERE `pid`=?')
            ->execute($dc->id)
        ;
    }

    /**
     * beim löschen einer Akte auch den Ordner mit allen Files zur Akte löschen.
     */
    public function dropCaseFolder(DataContainer $dc): void
    {
        $objCase = LcpCaseModel::findByPk($dc->id);
        if (null === $objCase) {
            return;
        }

        $uploadPath = $objCase->caseFolder;
        $logger = System::getContainer()->get('monolog.logger.contao');

        try {
            $objFolder = new Folder($uploadPath);
            $objFolder->purge();
            $objFolder->delete();

            // Add a log entry
            $logger->log(
                LogLevel::INFO,
                'Purged the LCP Upload-Folder',
                ['contao' => new ContaoContext(__METHOD__, TL_GENERAL)]
            );
        } catch (\Exception $e) {
            // Add a log entry
            $logger->log(
                LogLevel::ERROR,
                'NO Purged the LCP Upload-Folder:'.$uploadPath,
                ['contao' => new ContaoContext(__METHOD__, TL_ERROR)]
            );
        }
    }

    /**
     * der Listen-Ansicht der Fälle bei einer mehrfach-Bearbeitung
     * den zusaetzlichen Button für die Zip-generierung hinzufuegen.
     *
     * @param $arrButtons
     */
    public function addZipButton($arrButtons, DataContainer $dc)
    {
        $arrButtons['createZips'] =
            '<button type="submit" name="createZips" id="createZips" class="tl_submit" accesskey="g">'.
            $GLOBALS['TL_LANG']['MSC']['createZips'].
            '</button>';

        return $arrButtons;
    }

    /**
     * wird nach dem Klick des Zip-Icons in der Fall-Liste ausgeführt und gibt eine zip-Datei zum Download zurück.
     *
     * @throws \Exception
     */
    public function getCaseAsZip(DataContainer $dc): void
    {
        if ('compress' === !Input::get('key')) {
            return;
        }

        //URL ohne den key=compress -String
        $cleanReferer = str_replace('&key=compress', '', Controller::getReferer());

        //Case holen
        $objCase = LcpCaseModel::findByPk($dc->id);
        if (null === $objCase) {
            Controller::redirect($cleanReferer);
        }

        // Zip erzeugen
        $CaseHelper = new CaseHelper();
        $zipPath = $CaseHelper->createOneCaseToZip($objCase);

        if (file_exists(TL_ROOT.\DIRECTORY_SEPARATOR.$zipPath)) {
            Controller::sendFileToBrowser($zipPath);
        } else {
            $msg = sprintf($GLOBALS['TL_LANG']['tl_lcp_case']['zip_not_exist'], TL_ROOT.'/'.$zipPath);
            System::getContainer()
                ->get('monolog.logger.contao')
                ->log(LogLevel::INFO, $msg)
            ;
            Message::addInfo($msg);

            Controller::redirect($cleanReferer);
        }
    }

    public function multipleCaseExport(DataContainer $dc): void
    {
        $zipButton = Input::post('createZips');
        $arrCaseId = Input::post('IDS');

        // nur weiter wenn der Button geklickt wurde und es Archive gibt die zur Versendung geklickt wurden
        if (null === $zipButton) {
            return;
        }

        System::loadLanguageFile('tl_lcp_case');
        $CaseHelper = new CaseHelper();
        $ArrZipPath = [];

        // Zip-Dateien erstellen und deren Pfade im Array sammeln
        $zipPath = $CaseHelper->createMultiCaseToZip($arrCaseId);
        if (file_exists(TL_ROOT.\DIRECTORY_SEPARATOR.$zipPath)) {
            Controller::sendFileToBrowser($zipPath);
        } else {
            $msg = sprintf($GLOBALS['TL_LANG']['tl_lcp_case']['zip_not_exist'], TL_ROOT.'/'.$zipPath);
            System::getContainer()
                ->get('monolog.logger.contao')
                ->log(LogLevel::INFO, $msg)
            ;
            Message::addInfo($msg);
            Input::setPost('createZips', null);
            Controller::reload();
        }
    }
}
