<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\EventListener\Dca;

use Contao\Controller;
use Contao\FilesModel;
use Contao\Input;
use Srhinow\LawyerClientPortal\Model\LcpSettingModel;

class LcpSetting
{
    /**
     * all dca config > onload_callback entries.
     */
    public function onLoadCallback(): void
    {
        self::createPropertyEntry();
    }

    /**
     * create an entry if id=1 not exists.
     */
    public static function createPropertyEntry(): void
    {
        $count = LcpSettingModel::countAll();
        if (0 === $count) {
            $objSettings = new LcpSettingModel();
            $objSettings->id = $GLOBALS['LAWYERCLIENTPORTAL_SETTINGS']['ID'];
            $objSettings->save();
        }
    }

    /**
     * gibt alle Settings als Objekt zurück.
     *
     * @return LcpSettingModel
     */
    public static function getLcpSettings()
    {
        $objSettings = LcpSettingModel::findByPk($GLOBALS['LAWYERCLIENTPORTAL_SETTINGS']['ID']);
        if (null === $objSettings) {
            self::createPropertyEntry();
            Controller::reload();
        }

        return $objSettings;
    }

    /**
     * gibt den Temp-Pfad aus den Einstellungen zurueck.
     *
     * @return string
     */
    public static function getLcpTempPath()
    {
        $objSettings = self::getLcpSettings();

        //den Akten-Root-Pfad aus den Settings holen
        $objFolder = FilesModel::findByUuid($objSettings->tmpFolder);

        return (null === $objFolder || !$objFolder->path) ? 'var/cache' : $objFolder->path;
    }

    /**
     * gibt den Upload-Pfad aus den Einstellungen zurueck.
     *
     * @return string
     */
    public static function getLcpUploadPath()
    {
        $objSettings = self::getLcpSettings();

        //den Akten-Root-Pfad aus den Settings holen
        $objFolder = FilesModel::findByUuid($objSettings->uploadFolder);
        $FolderPath = $objFolder->path;

        if(null !== ($recordId = Input::get('id'))) {
            $FolderPath .= '/'.$recordId;
        }

        return (null === $objFolder || !$FolderPath) ? 'var/cache' : $FolderPath;
    }
}
