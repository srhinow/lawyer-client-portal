<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\EventListener\Dca;

use Contao\BackendUser;
use Contao\Database;
use Contao\Input;
use Contao\MemberModel;
use Contao\StringUtil;
use Srhinow\LawyerClientPortal\Model\LcpSettingModel;

class User
{
    /**
     * wird vom DCA aufgerufen um alle Mandanten zu holen die der Gruppe aus den Settings angehoeren.
     *
     * @return array
     */
    public function getLawyerUserAsOptions()
    {
        $options = [];

        //Settings holen
        $objSettings = LcpSettingModel::findByPk($GLOBALS['LAWYERCLIENTPORTAL_SETTINGS']['ID']);
        if (null === $objSettings || (int) $objSettings->userGroupId < 1) {
            return $options;
        }

        // Anwaelte holen die der Gruppe aus den Settings angehoeren
        $objUsers = Database::getInstance()
            ->execute('SELECT * FROM tl_user WHERE `groups` LIKE \'%"'.$objSettings->userGroupId.'"%\'')
        ;
        if ($objUsers->numRows < 1) {
            return $options;
        }

        //Member in Options sammeln
        while ($objUsers->next()) {
            $options[$objUsers->id] = $objUsers->name;
        }

        return $options;
    }

    public function getAllActiveMemberAsOptions()
    {
        $objMembers = MemberModel::findAll();
        $options = [];

        if (null === $objMembers) {
            return $options;
        }

        foreach ($objMembers as $member) {
            $options[$member->id] = $member->firstname.' '.$member->lastname.' _ '.$member->email;
        }

        return $options;
    }

    /**
     * Get all content elements and return them as array (content element alias).
     *
     * @return array
     */
    public function getAlias()
    {
        $arrPids = [];
        $arrAlias = [];
        $User = BackendUser::getInstance();
        $DB = Database::getInstance();

        if (!$User->isAdmin) {
            foreach ($User->pagemounts as $id) {
                $arrPids[] = [$id];
                $arrPids[] = $DB->getChildRecords($id, 'tl_page');
            }

            if (!empty($arrPids)) {
                $arrPids = array_merge(...$arrPids);
            } else {
                return $arrAlias;
            }

            $objAlias = $DB->prepare("SELECT c.id, c.pid, c.type, 
       (CASE c.type WHEN 'module' THEN m.name WHEN 'form' THEN f.title WHEN 'table' THEN c.summary ELSE c.headline END) 
           AS headline, c.text, a.title 
        FROM tl_content c 
            LEFT JOIN tl_article a ON a.id=c.pid 
            LEFT JOIN tl_module m ON m.id=c.module 
            LEFT JOIN tl_form f on f.id=c.form 
        WHERE a.pid IN(".implode(',', array_map('\intval', array_unique($arrPids))).") 
        AND (c.ptable='tl_article' OR c.ptable='') AND c.id!=? 
        ORDER BY a.title, c.sorting")
                ->execute(Input::get('id'))
            ;
        } else {
            $objAlias = $DB->prepare("SELECT c.id, c.pid, c.type, 
       (CASE c.type WHEN 'module' THEN m.name WHEN 'form' THEN f.title WHEN 'table' THEN c.summary ELSE c.headline END) 
           AS headline, c.text, a.title 
        FROM tl_content c 
            LEFT JOIN tl_article a ON a.id=c.pid 
            LEFT JOIN tl_module m ON m.id=c.module 
            LEFT JOIN tl_form f on f.id=c.form 
        WHERE (c.ptable='tl_article' OR c.ptable='') 
          AND c.id!=? 
        ORDER BY a.title, c.sorting")
                ->execute(Input::get('id'))
            ;
        }

        while ($objAlias->next()) {
            $arrHeadline = StringUtil::deserialize($objAlias->headline, true);

            if (isset($arrHeadline['value'])) {
                $headline = StringUtil::substr($arrHeadline['value'], 32);
            } else {
                $headline = StringUtil::substr(preg_replace('/[\n\r\t]+/', ' ', $arrHeadline[0]), 32);
            }

            $text = StringUtil::substr(strip_tags(preg_replace('/[\n\r\t]+/', ' ', $objAlias->text)), 32);
            $strText = $GLOBALS['TL_LANG']['CTE'][$objAlias->type][0].' (';

            if ('' !== $headline) {
                $strText .= $headline.', ';
            } elseif ('' !== $text) {
                $strText .= $text.', ';
            }

            $key = $objAlias->title.' (ID '.$objAlias->pid.')';
            $arrAlias[$key][$objAlias->id] = $strText.'ID '.$objAlias->id.')';
        }

        return $arrAlias;
    }
}
