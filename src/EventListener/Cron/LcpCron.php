<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\EventListener\Cron;

use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\MemberModel;
use Contao\System;
use Psr\Log\LogLevel;
use Srhinow\LawyerClientPortal\EventListener\Dca\LcpSetting;
use Srhinow\LawyerClientPortal\Helper\Helper;
use Srhinow\LawyerClientPortal\Model\LcpCaseModel;
use Srhinow\LawyerClientPortal\Model\LcpCaseRecordModel;

class LcpCron
{
    /**
     * taeglicher CronJob.
     */
    public function onDaily(): void
    {
    }

    /**
     * stuendlicher CronJob.
     */
    public function onHourly(): void
    {
    }

    /**
     * menuetlicher CronJob.
     */
    public function onMinutely(): void
    {
        $logger = System::getContainer()->get('monolog.logger.contao');
        $logger->log(
            LogLevel::INFO,
            'run LCP - onMinutely - Cron',
            ['contao' => new ContaoContext(__METHOD__, TL_CRON)]
        );
        $this->sendQueuedRecords();
        $this->deaktivateCase();
    }

    /**
     * sendet alle zum Versand vorgemerkten Nachrichten.
     */
    protected function sendQueuedRecords(): void
    {
        $objSettings = LcpSetting::getLcpSettings();

        $cronTime = \strlen($objSettings->cronTime) ? $objSettings->cronTime : '00:00';
        $cronBeginnTime = strtotime(date('Y-m-d').' '.$cronTime);
        $now = time();

        if ($now >= $cronBeginnTime) {
            $objRecords = LcpCaseRecordModel::findBy(
                ['readyToSend = ?', 'sendByDate <= ?', 'dateSend IS NULL'],
                ['1', $now]
            );

            if (null === $objRecords) {
                return;
            }

            $memberIds = [];

            while ($objRecords->next()) {
                $objCase = LcpCaseModel::findByPk($objRecords->pid);
                if (null === $objCase) {
                    continue;
                }

                $objMember = MemberModel::findByPk($objCase->memberId);
                if (null === $objMember) {
                    continue;
                }

                //Notification nur einmal senden auch wenn mehrere Nachrichten fuer Ihn versendet werden
                if (!\in_array($objMember->id, $memberIds, true)) {
                    $memberIds[] = $objMember->id;
                    $arrToken = ['send_to' => $objMember->email];
                    Helper::sendNotification($objSettings->newRecordNotification, $arrToken);
                }

                $objRecords->new = 1;
                $objRecords->readyToSend = '';
                $objRecords->dateSend = $now;
                $objRecords->sendToId = $objMember->id;
                $objRecords->sendToEmail = $objMember->email;
                $objRecords->save();
            }
        }
    }

    /**
     * deaktiviert eine Akte wenn ein Stop-Datum gesetzt wurde.
     */
    protected function deaktivateCase(): void
    {
        $objMarketCases = LcpCaseModel::findBy(['stop !=?', 'published=?'], ['', '1']);

        if (null === $objMarketCases) {
            return;
        }

        $objSettings = LcpSetting::getLcpSettings();

        while ($objMarketCases->next()) {
            $strtotime = '+'.$objSettings->closedAfterDays.' days';
            $closedDay = strtotime($strtotime, (int) $objMarketCases->stop);

            if ($closedDay <= time()) {
                $objMarketCases->published = '';
                $objMarketCases->save();
            }
        }
    }
}
