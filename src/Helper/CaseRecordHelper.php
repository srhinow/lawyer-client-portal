<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\Helper;

use Contao\Controller;
use Contao\Database;
use Contao\DataContainer;
use Contao\Dbafs;
use Contao\Files;
use Contao\FilesModel;
use Contao\Frontend;
use Contao\FrontendUser;
use Contao\Input;
use Contao\UserModel;
use Srhinow\LawyerClientPortal\EventListener\Dca\LcpSetting;
use Srhinow\LawyerClientPortal\Model\LcpCaseModel;
use Srhinow\LawyerClientPortal\Model\LcpCaseRecordModel;

class CaseRecordHelper
{
    /**
     * @param $actionItems
     * @param bool $wasRead
     */
    public function changeReadMultiRecords($actionItems, $wasRead = false): void
    {
        if (!\is_array($actionItems) || \count($actionItems) < 1) {
            return;
        }

        $FeMember = FrontendUser::getInstance();

        foreach ($actionItems as $item) {
            $objRecord = LcpCaseRecordModel::findOneBy(
                ['id=?', 'memberId=?', 'authorTable=?'],
                [$item, $FeMember->id, 'tl_user']
            );
            if (null === $objRecord) {
                continue;
            }

            $objRecord->new = ($wasRead) ? '' : '1';
            $objRecord->save();
        }
    }

    /**
     * sammelt alle Lawyer die zu dem Member gehoeren.
     *
     * @return array
     */
    public function getLawyerSelectOptions()
    {
        $arrMember1 = $this->getAllLawyerFromCases();
        $arrMember2 = $this->getAllLawyerFromRecords();
        $arrReturn = $arrMember1;

        if (\is_array($arrMember2) && \count($arrMember2) > 0) {
            foreach ($arrMember2 as $k => $entry) {
                if (\array_key_exists($k, $arrReturn)) {
                    continue;
                }
                $arrReturn[$k] = $entry;
            }
        }

        return $arrReturn;
    }

    /**
     * die jeweils zustaendigen Anwalt aus den Akten ermitteln
     * und als Select-Array zurück geben.
     *
     * @return array
     */
    public function getAllLawyerFromCases()
    {
        $arrLawyer = [];
        $Member = FrontendUser::getInstance();

        $options['group'] = 'userId';
        $objCases = LcpCaseModel::findBy(['memberId=?'], [$Member->id], $options);
        if (null === $objCases) {
            return $arrLawyer;
        }

        while ($objCases->next()) {
            $objUser = UserModel::findByPk($objCases->userId);
            if (null === $objUser) {
                continue;
            }
            $arrLawyer[$objUser->id] = $objUser->name;
        }

        return $arrLawyer;
    }

    /**
     * gibt alle zum Client gehörenden Anwaelte die bisher bereits
     * Kontakt aufgenommen hatten und als Select-Array zurück geben.
     *
     * @return array
     */
    public function getAllLawyerFromRecords()
    {
        $arrLawyer = [];
        $Member = FrontendUser::getInstance();

        $options['group'] = 'userId';
        $objRecords = LcpCaseRecordModel::findBy(
            ['memberId=?', 'authorTable=?'],
            [$Member->id, 'tl_user'],
            $options
        );
        if (null === $objRecords) {
            return $arrLawyer;
        }

        while ($objRecords->next()) {
            $objUser = UserModel::findByPk($objRecords->userId);
            $arrLawyer[$objUser->id] = $objUser->name;
        }

        return $arrLawyer;
    }

    /**
     * loescht mehrere Nachrichten wenn man der Eigner ist.
     *
     * @param $actionItems
     */
    public function deleteMultiRecords($actionItems): void
    {
        if (!\is_array($actionItems) || \count($actionItems) < 1) {
            return;
        }

        $FeMember = FrontendUser::getInstance();

        foreach ($actionItems as $item) {
            $objRecord = LcpCaseRecordModel::findOneBy(
                ['id=?', 'memberId=?', 'authorTable=?'],
                [$item, $FeMember->id, 'tl_member']
            );
            if (null === $objRecord) {
                continue;
            }

            $objRecord->deleted = '1';
            $objRecord->save();
        }

        Controller::reload();
    }

    /**
     * löscht den Nachrichten-Entwurf.
     *
     * @return bool
     */
    public function deleteDraftRecordFormActions()
    {
        $RecordId = (int) Input::post('recordId');
        $DropDraftBtn = Input::post('dropdraft');

        if (null === $DropDraftBtn || $RecordId < 1) {
            return false;
        }

        $objDraftRecord = LcpCaseRecordModel::findOneBy(['`id`=?', 'draft=?'], [$RecordId, '1']);
        if (null === $objDraftRecord) {
            return false;
        }

        Database::getInstance()->prepare('DELETE FROM tl_lcp_case_record WHERE id=?')->execute($RecordId);

        return true;
    }

    /**
     * speichert den Nachrichten-Entwurf.
     *
     * @return bool
     */
    public function saveNewRecordFormActions()
    {
        $btnSend = Input::post('send');
        $btnDraft = Input::post('draft');
        $dCounts = (int) Input::post('dokumente');
        $attachFiles = json_decode(Input::post('attachfiles'), true);

        if (null === $btnSend && null === $btnDraft) {
            return false;
        }

        $Member = FrontendUser::getInstance();

        $RecordId = Input::post('recordId');
        if ('' === $RecordId) {
            $objRecord = new LcpCaseRecordModel();
        } else {
            $objRecord = LcpCaseRecordModel::findOneBy(['`id`=?', 'draft=?'], [$RecordId, '1']);
            if (null === $objRecord) {
                return false;
            }
        }

        $objRecord->pid = Input::post('case');
        $objRecord->tstamp = time();
        $objRecord->dateAdded = time();
        $objRecord->userId = Input::post('lawyer');
        $objRecord->memberId = $Member->id;
        $objRecord->authorId = $Member->id;
        $objRecord->authorName = $Member->firstname.' '.$Member->lastname;
        $objRecord->authorTable = 'tl_member';
        $objRecord->subject = Input::post('subject');
        $objRecord->text = html_entity_decode(Input::post('message'));
        $objRecord->new = '1';

        if (null === $btnDraft) {
            $objRecord->sendToId = Input::post('lawyer');
            $objRecord->dateSend = time();
            $objRecord->draft = '';

            //Uploads
            if ((int) $dCounts > 0 && \is_array($attachFiles) && \count($attachFiles) > 0) {
                try {
                    $objRecord->upload = $this->convertFileArrayForDatabase(
                        $this->moveUploadsToCaseFolder($attachFiles)
                    );
                } catch (\Exception $e) {
                }
            } else {
                $objRecord->upload = null;
            }

            //damit der "neue Nachrichten" - Bereich geschlossen bleibt
            setcookie('LCP_NEW_RECORD', '', time() - 1000000);
            unset($_COOKIE['LCP_NEW_RECORD']);
        } else {
            $objRecord->draft = '1';

            //Uploads
            if ((int) $dCounts > 0 && \is_array($attachFiles) && \count($attachFiles) > 0) {
                $objRecord->upload = $attachFiles;
            } else {
                $objRecord->upload = null;
            }

            //damit der "neue Nachrichten" - Bereich geschlossen bleibt
            setcookie('LCP_NEW_RECORD', '1', time() + 1000000);
        }

        $objRecord->save();

        return true;
    }

    /**
     * @param string $uploadField
     *
     * @return array|bool
     */
    public function getLinksFromUpload($uploadField)
    {
        $arrFiles = [];

        if (null === $uploadField) {
            return $arrFiles;
        }

        $arrUploads = \is_array($uploadField) ? $uploadField : unserialize($uploadField);

        if (!\is_array($arrUploads) || \count($arrUploads) < 1) {
            return $arrFiles;
        }

        foreach ($arrUploads as $k => $upload) {
            if (!\strlen($upload)) {
                continue;
            }

            $objFile = \FilesModel::findByUuid($upload);

            if (null === $objFile) {
                continue;
            }

            $arrFiles[] = [
                'showFile' => Frontend::addToUrl('do=showfile&file='.$objFile->id),
                'name' => $objFile->name,
                'extension' => $objFile->extension,
                'path' => $objFile->path,
            ];
        }

        return (\count($arrFiles) < 0) ? false : $arrFiles;
    }

    /**
     * verschiebt die per DropZone hochgeladene Datei aus den upload-Folder in den passenden Case-Ordner.
     *
     * @param array $arrFiles
     *
     * @throws \Exception
     *
     * @return array
     */
    public function moveUploadsToCaseFolder($arrFiles = [])
    {
        $arrReturn = [];
        if (!\is_array($arrFiles) || \count($arrFiles) < 1) {
            return $arrReturn;
        }

        $caseId = (int) Input::post('case');
        $objCase = LcpCaseModel::findByPk($caseId);
        if (null === $objCase) {
            return $arrReturn;
        }

        $Member = FrontendUser::getInstance();
        $objSettings = LcpSetting::getLcpSettings();
        $uploadPath = FilesModel::findByUuid($objSettings->uploadFolder)->path;
        $Files = Files::getInstance();

        foreach ($arrFiles as $file) {
            $srcFolder = $uploadPath.'/'.$Member->id.'/'.$file;
            $dstFolder = $objCase->caseFolder.'/'.$file;

            if (!$Files->rename($srcFolder, $dstFolder)) {
                continue;
            }

            //in Contao-Files-Logik aufnehmen
            Dbafs::addResource($dstFolder);

            $arrReturn[] = $dstFolder;
        }

        return $arrReturn;
    }

    /**
     * Konvertiert die Dateipfade in uuid's für die upload-Datenbankspalte.
     *
     * @param array $arrFiles
     *
     * @return array
     */
    public function convertFileArrayForDatabase($arrFiles = [])
    {
        $arrReturn = [];
        if (!\is_array($arrFiles) || \count($arrFiles) < 1) {
            return $arrReturn;
        }

        foreach ($arrFiles as $FilePath) {
            $objFile = FilesModel::findByPath($FilePath);
            if (null === $objFile) {
                continue;
            }

            $arrReturn[] = $objFile->uuid;
        }

        return $arrReturn;
    }

    /**
     * Konvertiert die Uuid's in Dateipfade für die upload-Datenbankspalte.
     *
     * @param array $arrFiles
     *
     * @return array
     */
    public function convertFileArrayForJson($arrFiles = [])
    {
        $arrReturn = [];
        if (!\is_array($arrFiles) || \count($arrFiles) < 1) {
            return $arrReturn;
        }

        foreach ($arrFiles as $uuid) {
            $objFile = FilesModel::findByUuid($uuid);
            if (null === $objFile) {
                continue;
            }

            $arrReturn[] = basename($objFile->path);
        }

        return $arrReturn;
    }

    public function setRecordAsRead($recordId = null): void
    {
        if (null === $recordId || (int) $recordId < 1) {
            $response = ['status' => 'error', 'msg' => 'recordId "'.$recordId.'" stimmt nicht.'];
            echo json_encode($response);
            exit();
        }

        $objRecord = LcpCaseRecordModel::findByPk((int) $recordId);
        if (null === $objRecord) {
            $response = ['status' => 'error', 'msg' => 'kein Record-Datensatz zu "'.$recordId.'" gefunden.'];
            echo json_encode($response);
            exit();
        }

        $objRecord->dateOpen = time();
        $objRecord->new = '';
        $objRecord->save();

        $response = ['status' => 'success', 'msg' => 'Record "'.$recordId.'" als gelesen markieren.'];
        echo json_encode($response);
        exit();
    }

    /**
     * wenn GET-Parameter passen dann wird eine Datei zum Browser gesendet.
     */
    public function showCaseForMemberDocument(): void
    {
        if ('showfile' !== Input::get('do') || !$id = Input::get('file')) {
            return;
        }

        $FeMember = FrontendUser::getInstance();
        if(null === ($objCase = LcpCaseModel::findBy(['memberId=?'],[$FeMember->id]))) {
            return;
        }

        if(null === ($objFile = FilesModel::findByPk($id))) {
            return;
        }

        $fileDir = dirname($objFile->path);

        while ($objCase->next()) {
            if($objCase->caseFolder !== $fileDir) {
                return;
            }

            Controller::sendFileToBrowser($objFile->path);
        }
    }
}
