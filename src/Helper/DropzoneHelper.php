<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\Helper;

use Contao\Dbafs;
use Contao\File;
use Contao\Files;
use Contao\Folder;
use Contao\FrontendUser;
use Contao\StringUtil;
use Srhinow\LawyerClientPortal\Automator\LcpAutomator;
use Srhinow\LawyerClientPortal\EventListener\Dca\LcpSetting;

class DropzoneHelper
{
    public $tmpFolder = '';

    public $uploadFolder = '';

    public $extensions = '.jpg,.jpeg,.png,.pdf';

    public $fileSize = 5242880;

    public $fileName = '';
    protected $files;

    public function __construct()
    {
        $this->setFromSettings();
    }

    /**
     * nimmt den erlaubten Extension-String auseinander und gibt ihn als Array zurueck.
     *
     * @return array
     */
    public function getExtensionArray()
    {
        $array = explode(',', strtolower($this->extensions));

        return \is_array($array) ? $array : [];
    }

    /**
     * bereinigt den Tmp-Ordner.
     */
    public function cleanTmpFolder(): void
    {
        $automator = new LcpAutomator();
        $automator->cleanCaseTempFolder();
    }

    /**
     * bereinigt den Upload-Ordner.
     */
    public function cleanUploadFolder(): void
    {
        $automator = new LcpAutomator();
        $automator->cleanUploadFolder();
    }

    public function cleanFileName($name)
    {
        $name = StringUtil::sanitizeFileName($name);

        return str_replace(' ', '_', $name);
    }

    /**
     * prueft die Datei-Erweiterung.
     *
     * @return bool
     */
    public function checkExtensions(array $file)
    {
        $path_parts = pathinfo($file['name']);

        //Wenn keine Dateierweiterung uebrgeben wurde -> abbrechen
        if (\strlen($path_parts['extension']) < 1) {
            return false;
        }

        //Wenn die Datei-Erweiterung nicht erlaubt ist -> abbrechen
        if (!\in_array('.'.strtolower($path_parts['extension']), $this->getExtensionArray(), true)) {
            return false;
        }

        //wenn alles passt
        return true;
    }

    /**
     * prueft die Dateigroesse.
     *
     * @return bool
     */
    public function checkFileSize(array $file)
    {
        $file['size'] = (int) $file['size'];

        //wenn die Dateigroeße kleiner als 1 byte ist stimmt was nicht
        if ($file['size'] < 1) {
            return false;
        }

        //wenn die Dateigroesse mehr als erlaubt ist
        if ($file['size'] > ceil($this->fileSize * 1024 * 1024)) {
            return false;
        }

        //wenn alles passt
        return true;
    }

    /**
     * @param string $status
     * @param string $message
     * @param array  $array2
     */
    public function setAjaxResponse($status = 'error', $message = '', $array2 = []): void
    {
        echo json_encode(
            array_merge(
                [
                    'status' => $status,
                    'message' => $message,
                ],
                $array2
            )
        );
        die();
    }

    /**
     * per Ajax Datei im System ablegen.
     *
     * @param null $files
     *
     * @throws \Exception
     *
     * @return false|string
     */
    public function upload($files = null)
    {
        $this->files = (null === $files) ? $_FILES : $files;
        if (!\is_array($this->files)) {
            $this->setAjaxResponse('error', 'no_files');
        }

        $FeUser = FrontendUser::getInstance();

        //alles bereinigen
//        $this->cleanUploadFolder();

        $arrFiles = [];

        foreach ($this->files as $file) {
            // auf passende Datei-Erweiterung pruefen
            if (!$this->checkExtensions($file)) {
                $this->setAjaxResponse(
                    'error',
                    'extension',
                    ['file' => $file]
                );
            }

            // auf Dateigroesse pruefen
            if (!$this->checkFileSize($file)) {
                $this->setAjaxResponse(
                    'error',
                    'filesize',
                    ['file' => $file]
                );
            }

            // Dateiname bereinigen
            $this->fileName = $this->cleanFileName($file['name']);

            //Ziel-Pfad mit FrontendUser-ID als Unterverzeichnis
            $targetPath = $this->uploadFolder.'/'.$FeUser->id;
            if (!file_exists($targetPath)) {
                new Folder($targetPath);
                \Dbafs::addResource($targetPath);
            }

            $targetPathAbs = $targetPath.'/'.$this->fileName;

            //Datei in Ziel-Pfad kopieren
            $Files = Files::getInstance();
            if (!$Files->move_uploaded_file($file['tmp_name'], $targetPathAbs)) {
                $this->setAjaxResponse(
                    'error',
                    'move_uploaded_file',
                    ['file' => $targetPathAbs]
                );
            }

            //Berechtigung setzen
            $Files->chmod($targetPathAbs, 0666 & ~umask());

            //pruefen ob die Datei dort existiert wo sie hinkopiert wurde
            if (!file_exists(TL_ROOT.'/'.$targetPathAbs)) {
                $this->setAjaxResponse(
                    'error',
                    'file_not_exist',
                    ['file' => $targetPathAbs]
                );
            }

            //in Contao-Files-Logik aufnehmen
            Dbafs::addResource($targetPathAbs);

            $arrFiles[] = $this->fileName;
        }

        //nach dem hochladen aller Files ohne Fehler -> success
        $this->setAjaxResponse(
            'success',
            'files_uploadet',
            ['files' => $arrFiles]
        );
    }

    public function remove($fileName): void
    {
        $FeUser = FrontendUser::getInstance();

        // Dateiname bereinigen und auf den gleichen Stand bringen wie im System abgelegt
        $this->fileName = $this->cleanFileName($fileName);

        $filePath = $this->uploadFolder.'/'.$FeUser->id.'/'.$this->fileName;

        //pruefen ob die Datei dort existiert wo sie hinkopiert wurde
        if (!file_exists(TL_ROOT.'/'.$filePath)) {
            $this->setAjaxResponse(
                'error',
                'file_not_exist',
                ['path' => $filePath, 'file' => $this->fileName]
            );
        }

        //pruefen ob die der Pfad ein Ordner und keine Datei ist
        if (is_dir(TL_ROOT.'/'.$filePath)) {
            $this->setAjaxResponse(
                'error',
                'is_a_dir',
                ['path' => $filePath, 'file' => $this->fileName]
            );
        }

        try {
            (new File($filePath))->delete();
        } catch (\Exception $e) {
            $this->setAjaxResponse(
                'error',
                'delete_failed',
                ['path' => $filePath, 'file' => $this->fileName]
            );
        }
        $this->setAjaxResponse(
            'success',
            'file_deleted',
            ['path' => $filePath, 'file' => $this->fileName]
        );
    }

    /**
     * ueberschreibt/ setzt die Paramater mit den aus den LCP-Einstellungen.
     */
    protected function setFromSettings(): void
    {
        $objSettings = LcpSetting::getLcpSettings();
        if (null === $objSettings) {
            return;
        }

        $this->tmpFolder = \FilesModel::findByUuid($objSettings->tmpFolder)->path;
        $this->uploadFolder = \FilesModel::findByUuid($objSettings->uploadFolder)->path;
        $this->extensions = $objSettings->uploadFileExtensions;
        $this->fileSize = $objSettings->uploadFileSize;
    }
}
