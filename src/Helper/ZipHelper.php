<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\Helper;

use Contao\Controller;
use Contao\CoreBundle\Monolog\ContaoContext;
use Psr\Log\LogLevel;

class ZipHelper
{
    public $subDir = '';
    protected $zipArchive;

    protected $zipFolder;

    protected $logger;

    public function __construct($zipArchive = '')
    {
        $this->logger = Controller::getContainer()->get('monolog.logger.contao');

        $this->zipArchive = new \ZipArchive();
        if (\strlen($zipArchive) > 0) {
            $this->openZipFolder($zipArchive);
        }
    }

    /**
     * @param $zipName
     *
     * @return bool
     */
    public function openZipFolder($zipName)
    {
        if (true === !$this->zipArchive->open($zipName, \ZipArchive::CREATE)) {
            $this->logger->log(
                LogLevel::ERROR,
                'konnte das Archiv '.$zipName.' nicht oeffnen.',
                ['contao' => new ContaoContext('openZipFolder', 'Srhinow\LawyerClientPortal\Helper\ZipHelper')]
            );

            return false;
        }

        return true;
    }

    public function closeZipFolder()
    {
        $this->zipArchive->close();

        return true;
    }

    /**
     * Function to recursively add a directory,
     * sub-directories and files to a zip archive.
     *
     * @param string $dir
     * @param string $subdir
     *
     * @return bool
     */
    public function addFolderToZip($dir = '', $subdir = '')
    {
        if (!is_dir($dir)) {
            $this->logger->log(
                LogLevel::ERROR,
                $dir.' ist kein Ordner',
                ['contao' => new ContaoContext('addFolderToZip', 'Srhinow\LawyerClientPortal\Helper\ZipHelper')]
            );

            return false;
        }

        if (!$dh = opendir($dir)) {
            $this->logger->log(
                LogLevel::ERROR,
                $dir.' konnte nicht geoeffnet werden.',
                ['contao' => new ContaoContext('addFolderToZip', 'Srhinow\LawyerClientPortal\Helper\ZipHelper')]
            );

            return false;
        }

        //Falls zum beispiel für jeden Fall ein Unterordner eingefuegt werden soll
        $subPath = '';
        if (\strlen($this->subDir) > 0) {
            $this->zipArchive->addEmptyDir($this->subDir);
            $subPath = $this->subDir.'/';
        }

        //Add the directory
        if (!empty($subdir)) {
            $this->zipArchive->addEmptyDir($subdir);
        }

        // Loop through all the files
        while (false !== ($file = readdir($dh))) {
            //If it's a folder, run the function again!
            if (!is_file($dir.'/'.$file)) {
                // Skip parent and root directories
                if (('.' !== $file) && ('..' !== $file)) {
                    $this->addFolderToZip($dir.'/'.$file, $subdir.'/'.$file);
                }
            } else {
                // Add the files
                $this->zipArchive->addFile($dir.'/'.$file, $subPath.$file);
            }
        }

        return true;
    }

    public function getSubDir(): string
    {
        return $this->subDir;
    }

    public function setSubDir(string $subDir): void
    {
        $this->subDir = $subDir;
    }
}
