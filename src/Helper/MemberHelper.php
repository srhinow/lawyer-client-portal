<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\Helper;

use Contao\CoreBundle\OptIn\OptIn;
use Contao\Environment;
use Contao\Idna;
use Contao\MemberModel;
use Contao\Message;
use Contao\PageModel;
use Contao\System;
use NotificationCenter\Model\Notification;
use Srhinow\LawyerClientPortal\Model\LcpSettingModel;

class MemberHelper
{
    protected $objSettings;

    public function __construct()
    {
        $this->setObjSettings();
    }

    /**
     * holt die LCP-Einstellungen.
     */
    public function setObjSettings(): void
    {
        $objSettings = LcpSettingModel::findByPk($GLOBALS['LAWYERCLIENTPORTAL_SETTINGS']['ID']);
        if (null === $objSettings) {
            return;
        }

        $this->objSettings = $objSettings;
    }

    /**
     * Send a lost password e-mail.
     *
     * @throws \Exception
     */
    public function sendTokenLink(MemberModel $objMember): void
    {
        // Notification-Instance setzen
        $objNotification = Notification::findByPk($this->objSettings->sendTokenNotification);
        if (null === $objNotification) {
            $msg = 'The notification was not found ID '.$this->objSettings->sendTokenNotification;
            Helper::log($msg, __METHOD__, TL_ERROR);

            return;
        }

        /** @var OptIn $optIn */
        $optIn = System::getContainer()->get('contao.opt-in');
        $optInToken = $optIn->create('pw', $objMember->email, ['tl_member' => [$objMember->id]]);

        $arrTokens = [];

        // erstelle alle Notifikation-Token
        foreach ($objMember->row() as $k => $v) {
            $arrTokens['member_'.$k] = $v;
        }

        $arrTokens['recipient_email'] = $objMember->email;
        $arrTokens['domain'] = Idna::decode(Environment::get('host'));
        $arrTokens['activation'] = $optInToken->getIdentifier();

        // Redirect to the jumpTo page
        if ($objTarget = PageModel::findByPk($this->objSettings->reg_jumpTo)) {
            /** @var PageModel $objTarget */
            $tokenUrl = $objTarget->getFrontendUrl();
        }
        $arrTokens['link'] = \Idna::decode(\Environment::get('base')).$tokenUrl.'?token='.$optInToken->getIdentifier();

        // Die Notifikation senden
        $objNotification->send($arrTokens, $GLOBALS['TL_LANGUAGE']);
        $msg = 'Die Aktivierungs-Email wurde an den Mandanten '.$objMember->id.' ('.$objMember->email.') versand.';

        // in System-Log schreiben
        Helper::log($msg, __METHOD__, TL_ACCESS);

        // als Info am oberen Rand im Backend setzen
        Message::addInfo($msg);
    }
}
