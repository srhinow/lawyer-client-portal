<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\Helper;

use Contao\BackendTemplate;
use Contao\Date;
use Contao\File;
use Contao\Folder;
use Contao\StringUtil;
use Contao\System;
use Psr\Log\LogLevel;
use Srhinow\LawyerClientPortal\EventListener\Dca\LcpSetting;
use Srhinow\LawyerClientPortal\Model\LcpCaseModel;
use Srhinow\LawyerClientPortal\Model\LcpCaseRecordModel;

class CaseHelper
{
    /**
     * erzeugt eine Zip-Datei mit allen benötigten Case-Inhalten.
     *
     * @param $objCase
     *
     * @throws \Exception
     *
     * @return string
     */
    public function createOneCaseToZip($objCase)
    {
        $objCaseRecords = LcpCaseRecordModel::findBy(['pid = ?', 'deleted != ?'], [$objCase->id, '1']);
        if (null === $objCaseRecords) {
            return '';
        }
        //alle Records in txt-Files im tmp-Ordner speichern
        $this->saveRecordsAsTxtInTemp($objCase);

        //das ZipFile erstellen
        $zipPath = $this->getCaseZipPath($objCase);
        $zip = new ZipHelper(TL_ROOT.\DIRECTORY_SEPARATOR.$zipPath);

        //alle Dateien im zip einfügen
        $zip->addFolderToZip(TL_ROOT.\DIRECTORY_SEPARATOR.$objCase->caseFolder);

        //alle Nachrichten im zip einfuegen
        $recordTempPath = $this->getCaseRecordTempPath($objCase);
        $zip->addFolderToZip(TL_ROOT.\DIRECTORY_SEPARATOR.$recordTempPath);

        $zip->closeZipFolder();

        return $zipPath;
    }

    /**
     * @param array $arrCaseId
     *
     * @throws \Exception
     *
     * @return string
     */
    public function createMultiCaseToZip($arrCaseId = [])
    {
        $zipPath = $this->getMultiZipPath();
        if (!\is_array($arrCaseId) || \count($arrCaseId) < 1) {
            return $zipPath;
        }

        $zip = new ZipHelper(TL_ROOT.\DIRECTORY_SEPARATOR.$zipPath);

        foreach ($arrCaseId as $id) {
            // Wenn es diesen Case nicht gibt die Schleife überspringen.
            $objCase = LcpCaseModel::findByPk($id);
            if (null === $objCase) {
                continue;
            }

            // Wenn es keine Nachrichten zu diesem Fall gibt, die Schleife überspringen.
            $objCaseRecords = LcpCaseRecordModel::findBy(['pid = ?', 'deleted != ?'], [$objCase->id, '1']);
            if (null === $objCaseRecords) {
                continue;
            }

            //alle Records in txt-Files im tmp-Ordner speichern
            $this->saveRecordsAsTxtInTemp($objCase);

            $cleanFolderNumber = Helper::cleanFolderNumber($objCase->caseNumber);
            $zip->subDir = $cleanFolderNumber;

            //alle Dateien im zip einfügen
            $zip->addFolderToZip(TL_ROOT.\DIRECTORY_SEPARATOR.$objCase->caseFolder);

            //alle Nachrichten im zip einfuegen
            $recordTempPath = $this->getCaseRecordTempPath($objCase);
            $zip->addFolderToZip(TL_ROOT.\DIRECTORY_SEPARATOR.$recordTempPath);
        }
        $zip->closeZipFolder();

        return $zipPath;
    }

    /**
     * alle Records als einzelene txt-Files in dem.
     *
     * @param $objCase
     *
     * @throws \Exception
     */
    public function saveRecordsAsTxtInTemp($objCase): void
    {
        if (null === $objCase || !\is_object($objCase)) {
            return;
        }

        //nur weiter wenn auch Gespraeche zu der Akte vorhanden sind
        $objCaseRecords = LcpCaseRecordModel::findBy(['pid = ?', 'deleted != ?'], [$objCase->id, '1']);
        if (null === $objCaseRecords) {
            return;
        }

        //Temp-Ordnerpfad setzen
        $tmpPath = $this->getCaseRecordTempPath($objCase);

        //Ordnerpfad erzeugen falls nicht vorhanden
        $this->createFolderIfNotExist($tmpPath);

        //Ordner leeren falls er schon existiert hat
        $objFolder = new Folder($tmpPath);
        $objFolder->purge();

        while ($objCaseRecords->next()) {
            // Inhalte generieren
            $Template = new BackendTemplate('zip_record');
            $Template->subject = $objCaseRecords->subject;
            $Template->authorName = $objCaseRecords->authorName;
            $Template->dateSend = $objCaseRecords->dateSend;
            $Template->caseNumber = $objCase->caseNumber;
            $Template->text = Helper::html2text($objCaseRecords->text);
            $FileContent = $Template->parse();

            // Record-Files erzeugen
            $filePath = $this->getRecordFilePath($objCase, $objCaseRecords);
            $file = new File($filePath);
            $file->write(str_replace('\n', "\n", $FileContent));
            $file->close();
        }
    }

    /**
     * gibt den CaseFolder wieder.
     *
     * @param $objCase
     *
     * @return string
     */
    public function getCaseTempPath($objCase)
    {
        $tmpPath = LcpSetting::getLcpTempPath();

        return $tmpPath.\DIRECTORY_SEPARATOR.$objCase->caseNumber;
    }

    /**
     * gibt den CaseFolder wieder.
     *
     * @param object $objCase
     *
     * @return string
     */
    public function getCaseRecordTempPath($objCase)
    {
        $tmpPath = $this->getCaseTempPath($objCase);

        return $tmpPath.\DIRECTORY_SEPARATOR.$GLOBALS['LAWYERCLIENTPORTAL_RECORD_FOLDER'];
    }

    /**
     * gibt den ZipPath wieder.
     *
     * @param object $objCase
     *
     * @return string
     */
    public function getCaseZipPath($objCase)
    {
        $tmpPath = LcpSetting::getLcpTempPath();
        $cleanFolderNumber = Helper::cleanFolderNumber($objCase->caseNumber);

        return $tmpPath.\DIRECTORY_SEPARATOR.$cleanFolderNumber.'_'.Date::parse('Y.m.d-His').'.zip';
    }

    /**
     * gibt den ZipPath für den Multi-Export wieder.
     *
     * @return string
     */
    public function getMultiZipPath()
    {
        $tmpPath = LcpSetting::getLcpTempPath();

        return $tmpPath.\DIRECTORY_SEPARATOR.'multiexport_'.Date::parse('Y.m.d-His').'.zip';
    }

    /**
     * @param $objCase
     * @param $objCaseRecords
     *
     * @return string
     */
    public function getRecordFilePath($objCase, $objCaseRecords)
    {
        $tmpPath = $this->getCaseRecordTempPath($objCase);

        return $tmpPath.\DIRECTORY_SEPARATOR.
            Date::parse(
                'Y.m.d-His',
                $objCaseRecords->dateSend
            ).'_'.StringUtil::substrHtml(
                StringUtil::standardize($objCaseRecords->subject),
                30
            ).'.txt';
    }

    /**
     * @param $folderPath
     *
     * @return bool
     */
    public function createFolderIfNotExist($folderPath)
    {
        // falls nicht vorhanden den Speicherort-Ordner anlegen
        if (!file_exists(TL_ROOT.\DIRECTORY_SEPARATOR.$folderPath)) {
            if (!mkdir(TL_ROOT.\DIRECTORY_SEPARATOR.$folderPath, 0755, true)) {
                System::getContainer()
                    ->get('monolog.logger.contao')
                    ->log(LogLevel::INFO, sprintf('Folder "%s" existiert nicht.', TL_ROOT.'/'.$folderPath))
                ;

                return false;
            }

            return true;
        }

        return true;
    }
}
