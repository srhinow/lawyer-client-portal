<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\LawyerClientPortal\Helper;

use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\System;
use NotificationCenter\Model\Notification;
use Psr\Log\LogLevel;

class Helper
{
    /**
     * Log-Routine.
     *
     * @param $strText
     * @param $strFunction
     * @param $strCategory
     */
    public static function log($strText, $strFunction, $strCategory): void
    {
        $level = TL_ERROR === $strCategory ? LogLevel::ERROR : LogLevel::INFO;
        $logger = System::getContainer()->get('monolog.logger.contao');

        $logger->log($level, $strText, ['contao' => new ContaoContext($strFunction, $strCategory)]);
    }

    /**
     * @param int $length
     *
     *@throws \Exception
     * @throws \Exception
     *
     * @return false|string
     */
    public static function uniqidReal($length = 35)
    {
        // uniqid gives 13 chars, but you could adjust it to your needs.
        if (\function_exists('random_bytes')) {
            $bytes = random_bytes((int) ceil($length / 2));
        } elseif (\function_exists('openssl_random_pseudo_bytes')) {
            $bytes = openssl_random_pseudo_bytes(ceil($length / 2));
        } else {
            throw new \Exception('no cryptographically secure random function available');
        }

        return substr(bin2hex($bytes), 0, $length);
    }

    /**
     * wandelt den HTML-String in einen normalen Text.
     *
     * @param string $html
     *
     * @return string|string[]|null
     */
    public static function html2text($html = '')
    {
        return preg_replace("/\n\\s+/", "\n", rtrim(html_entity_decode(strip_tags($html))));
    }

    /**
     * make CaseNumber ready for File/Foldername.
     *
     * @param string $folderNumber
     *
     * @return string|string[]
     */
    public static function cleanFolderNumber($folderNumber = '')
    {
        if (\strlen($folderNumber) < 1) {
            return $folderNumber;
        }

        return str_replace(['/', '\\', '*', '[', ']', '(', ')'], '-', $folderNumber);
    }

    public static function sendNotification($NotificationId, $arrToken)
    {
        $objNotification = Notification::findByPk($NotificationId);
        if (null === $objNotification) {
            return false;
        }

        $arrMessage = $objNotification->send($arrToken);

        if (!\is_array($arrMessage) || \count($arrMessage) < 1) {
            $msg = 'Die Notifocation konnte nicht gesendet werden.';
            $return = false;
        } else {
            $msg = 'Die Notification wurde erfolgreich versendet.';
            $return = true;
        }

        $level = ($return) ? LogLevel::INFO : LogLevel::ERROR;
        $logger = System::getContainer()->get('monolog.logger.contao');
        $logger->log($level, $msg, ['contao' => new ContaoContext(__METHOD__, ContaoContext::CRON)]);

        return $return;
    }
}
